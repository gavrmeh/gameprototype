// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */

struct DYNAMICVOXELTERRAIN_API FVoxel
{
	uint32 MaterialID;
	uint8 X;
	uint8 Y;
	uint8 Z;
	uint8 Flags;
	uint8 NormalX;
	uint8 NormalY;
	uint8 NormalZ;
};


class DYNAMICVOXELTERRAIN_API VoxelArray
{
public:
	VoxelArray();
	~VoxelArray();
};
