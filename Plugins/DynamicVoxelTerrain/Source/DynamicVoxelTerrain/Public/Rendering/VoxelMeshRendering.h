#pragma once

#include "CoreMinimal.h"
#include "LocalVertexFactory.h"
#include "StaticMeshResources.h"
#include "PrimitiveSceneProxy.h"
#include "Materials/MaterialInterface.h"
#include "Rendering/VoxelMeshData.h"
#include "Rendering/VoxelMeshBuffers.h"
#include "MeshMaterialShader.h"

class DYNAMICVOXELTERRAIN_API FVoxelMeshBuffers
{
public:
	
	FVoxelMeshBuffers(const FVoxelMeshSectionCreateData& InData);
	~FVoxelMeshBuffers();
	FVoxelMeshPositionVertexBuffer PositionsBuffer;
	FVoxelMeshTangentsVertexBuffer TangentsBuffer;
	FVoxelMeshTexCoordsVertexBuffer TexCoordsBuffer;
	FVoxelMeshColorVertexBuffer ColorsBuffer;
	FVoxelMeshIndexBuffer IndexBuffer;

	void UpdateData(const FVoxelMeshSectionUpdateData& Data);
	
	bool IsDynamic() const {return bDynamicBuffers;}
private:
	bool bDynamicBuffers = false;
};

static inline void InitOrUpdateResource(FRenderResource* Resource)
{
	if (!Resource->IsInitialized())
	{
		Resource->InitResource();
	}
	else
	{
		Resource->UpdateRHI();
	}
}

class DYNAMICVOXELTERRAIN_API FVoxelMeshSectionData
{
public:
	FVoxelMeshSectionData(ERHIFeatureLevel::Type FeatureLevel, const FVoxelMeshSectionCreateData& MeshData);
	~FVoxelMeshSectionData();
	FVoxelMeshBuffers Buffers;
	FLocalVertexFactory VertexFactory;
	UMaterialInterface* SectionMaterial;
	bool bIsVisible = true;
	void Update(const FVoxelMeshSectionUpdateData& MeshData);
	bool IsDynamic() const {return Buffers.IsDynamic(); }
	//bool HasVertecies() const {return bHasVerticies;}

private:
};

class UVoxelMeshComponent;

class DYNAMICVOXELTERRAIN_API FVoxelMeshSceneProxy : public FPrimitiveSceneProxy
{
public:
	FVoxelMeshSceneProxy(UVoxelMeshComponent* Component);
	virtual void CreateRenderThreadResources() override;
	virtual void DestroyRenderThreadResources() override;

	virtual void DrawStaticElements(FStaticPrimitiveDrawInterface* PDI) override;
	virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const override;

#if RHI_RAYTRACING
	virtual bool IsRayTracingRelevant() const override { return false; }
	virtual void GetDynamicRayTracingInstances(FRayTracingMaterialGatheringContext& Context, TArray<FRayTracingInstance>& OutRayTracingInstances) override;
#endif
	
	void CreateSection_RenderThread(uint16 Index, FVoxelMeshRenderUpdateData&& Data);
	void UpdateSection_RenderThread(uint16 Index, FVoxelMeshRenderUpdateData&& Data);
	void DeleteSection_RenderThread(uint16 Index);

	virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* View) const override;
	virtual bool CanBeOccluded() const override;
	virtual uint32 GetMemoryFootprint() const override;
	virtual SIZE_T GetTypeHash() const override;
	uint32 GetAllocatedSize() const;

	virtual bool HasDistanceFieldRepresentation() const override;
	virtual bool HasDynamicIndirectShadowCasterRepresentation() const override;
	//~ End FPrimitiveSceneProxy Interface

private:
	UVoxelMeshComponent* const Component;
	const FMaterialRelevance MaterialRelevance;
	const int32 LOD;

	const FCollisionResponseContainer CollisionResponse;
	const ECollisionTraceFlag CollisionTraceFlag;

	TMap<uint16, TSharedPtr<FVoxelMeshSectionData>> Sections;

	double FinishSectionsUpdatesTime = 0;
	double CreateSceneProxyTime = 0;
	mutable bool bLoggedTime = false;

	void DrawSection(
		FMeshBatch& Mesh,
		const TSharedPtr<FVoxelMeshSectionData>& FVoxelMeshSectionData,
		const FMaterialRenderProxy* MaterialRenderProxy,
		const FMaterialRenderProxy* WireframeMaterial,
		bool bEnableTessellation,
		bool bWireframe) const;

	bool ShouldDrawComplexCollisions(const FEngineShowFlags& EngineShowFlags) const;
};
