#pragma once

#include "CoreMinimal.h"
#include "Materials/MaterialInterface.h"
#include "StaticMeshResources.h"
#include "RenderResource.h"
#include "Engine/Engine.h"
struct DYNAMICVOXELTERRAIN_API FVoxelMeshTangentNormal
{
	FVoxelMeshTangentNormal(const FVector& InTangent, const FVector& InNormal)
	: Tangent(InTangent)
	, Normal(InNormal)
	{

	}
	FPackedNormal Tangent;
	FPackedNormal Normal;
};

struct DYNAMICVOXELTERRAIN_API FVoxelMeshData
{
	TArray<FVector> Positions;
	TArray<FVector> Normals;
	TArray<FVoxelMeshTangentNormal> Tangents;
	TArray<FVector2D> UVs;
	TArray<FColor> Colors;
	TArray<uint32> Indecies;
	bool bIsVisible = true;
	UMaterialInterface* Material = nullptr;

};


//Streams implementation from RMC
//Copyright(c) 2016 - 2020 TriAxis Games L.L.C.


struct DYNAMICVOXELTERRAIN_API FVoxelMeshVertexPositionStream
{


private:
	TArray<uint8> Data;

public:
	FVoxelMeshVertexPositionStream() { }

	FORCEINLINE int32 GetStride() const { return sizeof(FVector); }

	FORCEINLINE int32 Num() const
	{
		return Data.Num() / GetStride();
	}
	void SetNum(int32 NewNum, bool bAllowShrinking = true)
	{
		Data.SetNum(NewNum * GetStride(), bAllowShrinking);
	}
	void Reserve(int32 Number)
	{
		Data.Reserve(Number * GetStride());
	}
	void Empty(int32 Slack = 0)
	{
		Data.Empty(Slack * GetStride());
	}

	int32 Add(const FVector& InPosition)
	{
		int32 Index = Data.Num();
		Data.AddUninitialized(sizeof(FVector));
		*((FVector*)&Data[Index]) = InPosition;
		return Index / sizeof(FVector);
	}
	void Append(const FVoxelMeshVertexPositionStream& InOther)
	{
		Data.Append(InOther.Data);
	}
	void Append(const TArray<FVector>& InPositions)
	{
		int32 StartIndex = Data.Num();
		Data.SetNum(StartIndex + sizeof(FVector) * InPositions.Num());
		FMemory::Memcpy(Data.GetData() + StartIndex, InPositions.GetData(), InPositions.Num() * sizeof(FVector));
	}

	void SetPosition(int32 Index, const FVector& NewPosition)
	{
		*((FVector*)&Data[Index * sizeof(FVector)]) = NewPosition;
	}
	const FVector& GetPosition(int32 Index) const
	{
		return *((FVector*)&Data[Index * sizeof(FVector)]);
	}

	FBox GetBounds() const
	{
		FBox NewBox(ForceInit);
		int32 Count = Num();
		for (int32 Index = 0; Index < Count; Index++)
		{
			NewBox += GetPosition(Index);
		}
		return NewBox;
	}

	const uint8* GetData() const { return Data.GetData(); }
	TArray<uint8>&& TakeData()&& { return MoveTemp(Data); }
	const TArray<FVector> GetCopy() const
	{
		TArray<FVector> OutData;
		OutData.SetNum(Num());
		FMemory::Memcpy(OutData.GetData(), Data.GetData(), Data.Num());
		return OutData;
	}

	bool Serialize(FArchive& Ar)
	{
		Ar << Data;
		return true;
	}
};

template<> struct TStructOpsTypeTraits<FVoxelMeshVertexPositionStream> : public TStructOpsTypeTraitsBase2<FVoxelMeshVertexPositionStream>
{
	enum
	{
		WithSerializer = true
	};
};


struct DYNAMICVOXELTERRAIN_API FVoxelMeshVertexTangentStream
{


private:
	// This is always laid out tangent first, normal second, and it's 
	// either FPackedNormal for normal precision or FPackedRGBA16N for high precision.
	TArray<uint8> Data;

public:
	FVoxelMeshVertexTangentStream()
	{
	}

	FORCEINLINE bool IsHighPrecision() const { return false; }

	FORCEINLINE int32 GetElementSize() const { return sizeof(FPackedNormal); }
	FORCEINLINE int32 GetStride() const { return GetElementSize() * 2; }

	FORCEINLINE int32 Num() const
	{
		return Data.Num() / GetStride();
	}
	void SetNum(int32 NewNum, bool bAllowShrinking = true)
	{
		Data.SetNum(NewNum * GetStride(), bAllowShrinking);
	}
	void Reserve(int32 Number)
	{
		Data.Reserve(Number * GetStride());
	}
	void Empty(int32 Slack = 0)
	{
		Data.Empty(Slack * GetStride());
	}

	int32 Add(const FVector& InNormal, const FVector& InTangent)
	{
		int32 Index = Data.Num();
		int32 Stride = GetStride();
		Data.AddUninitialized(Stride);
		*((FPackedNormal*)&Data[Index]) = FPackedNormal(InTangent);
		*((FPackedNormal*)&Data[Index + sizeof(FPackedNormal)]) = FPackedNormal(InNormal);
		return Index / Stride;
	}
	int32 Add(const FVector4& InNormal, const FVector& InTangent)
	{
		int32 Index = Data.Num();
		int32 Stride = GetStride();
		Data.AddUninitialized(Stride);
		*((FPackedNormal*)&Data[Index]) = FPackedNormal(InTangent);
		*((FPackedNormal*)&Data[Index + sizeof(FPackedNormal)]) = FPackedNormal(InNormal);
		return Index / Stride;
	}
	int32 Add(const FVector& InTangentX, const FVector& InTangentY, const FVector& InTangentZ)
	{
		int32 Index = Data.Num();
		int32 Stride = GetStride();
		Data.AddUninitialized(Stride);
		*((FPackedNormal*)&Data[Index]) = FPackedNormal(InTangentX);
		*((FPackedNormal*)&Data[Index + sizeof(FPackedNormal)]) = FPackedNormal(FVector4(InTangentZ, GetBasisDeterminantSign(InTangentX, InTangentY, InTangentZ)));
		return Index / Stride;
	}
	void Append(const FVoxelMeshVertexTangentStream& InOther)
	{
		Data.Append(InOther.Data);
	}
	/*void Append(const TArray<FVector>& InNormals, const TArray<FVoxelMeshTangent>& InTangents)
	{
		int32 MaxCount = FMath::Max(InNormals.Num(), InTangents.Num());

		for (int32 Index = 0; Index < MaxCount; Index++)
		{
			const int32 NormalIndex = FMath::Min(Index, InNormals.Num());
			const int32 TangentIndex = FMath::Min(Index, InTangents.Num());

			FVector4 Normal;
			FVector Tangent;

			if (InNormals.IsValidIndex(NormalIndex))
			{
				Normal = InNormals[NormalIndex];
			}
			else
			{
				Normal = FVector(0, 0, 1);
			}

			if (InTangents.IsValidIndex(TangentIndex))
			{
				Tangent = InTangents[TangentIndex].TangentX;
				Normal.W = InTangents[TangentIndex].bFlipTangentY ? -1 : 1;
			}
			else
			{
				Tangent = FVector(1, 0, 0);
				Normal.W = 1.0f;
			}

			Add(Normal, Tangent);
		}
	}
	*/
	void Append(const TArray<FVoxelMeshTangentNormal>& Tangents)
	{
		size_t StartIndex = Data.Num();
		Data.SetNum(Data.Num() + sizeof(FVoxelMeshTangentNormal) * Tangents.Num());
		FMemory::Memcpy(Data.GetData() + StartIndex, Tangents.GetData(), Tangents.Num() * sizeof(FVoxelMeshTangentNormal));
	}
	void SetNormal(int32 Index, const FVector& NewNormal)
	{
		const int32 EntryIndex = Index * 2 + 1;
		*((FPackedNormal*)&Data[EntryIndex * sizeof(FPackedNormal)]) = FPackedNormal(NewNormal);
	}
	void SetTangent(int32 Index, const FVector& NewTangent)
	{
		const int32 EntryIndex = Index * 2;
		*((FPackedNormal*)&Data[EntryIndex * sizeof(FPackedNormal)]) = FPackedNormal(NewTangent);
	}
	void SetTangents(int32 Index, const FVector& InTangentX, const FVector& InTangentY, const FVector& InTangentZ)
	{
		const int32 EntryIndex = Index * 2;
		*((FPackedNormal*)&Data[EntryIndex * sizeof(FPackedNormal)]) = FPackedNormal(InTangentX);
		*((FPackedNormal*)&Data[(EntryIndex + 1) * sizeof(FPackedNormal)]) = FPackedNormal(FVector4(InTangentZ, GetBasisDeterminantSign(InTangentX, InTangentY, InTangentZ)));
	}

	FVector GetNormal(int32 Index) const
	{
		const int32 EntryIndex = Index * 2 + 1;
		return (*((FPackedNormal*)&Data[EntryIndex * sizeof(FPackedNormal)])).ToFVector();
	}
	FVector GetTangent(int32 Index) const
	{
		const int32 EntryIndex = Index * 2;
		return (*((FPackedNormal*)&Data[EntryIndex * sizeof(FPackedNormal)])).ToFVector();
	}
	void GetTangents(int32 Index, FVector& OutTangentX, FVector& OutTangentY, FVector& OutTangentZ) const
	{
		const int32 EntryIndex = Index * 2;
		FPackedNormal TempTangentX = *((FPackedNormal*)&Data[EntryIndex * sizeof(FPackedNormal)]);
		FPackedNormal TempTangentZ = *((FPackedNormal*)&Data[(EntryIndex + 1) * sizeof(FPackedNormal)]);
		OutTangentX = TempTangentX.ToFVector();
		OutTangentY = GenerateYAxis(TempTangentX, TempTangentZ);
		OutTangentZ = TempTangentZ.ToFVector();
	}

	const uint8* GetData() const { return Data.GetData(); }
	TArray<uint8>&& TakeData()&& { return MoveTemp(Data); }
	// 	const TArray<TArray<FVector2D>> GetCopy() const
	// 	{
	// 		TArray<FVector> OutData;
	// 		OutData.SetNum(Num());
	// 		FMemory::Memcpy(OutData.GetData(), Data.GetData(), Data.Num());
	// 		return OutData;
	// 	}

	bool Serialize(FArchive& Ar)
	{
		Ar << Data;
		return true;
	}
};

template<> struct TStructOpsTypeTraits<FVoxelMeshVertexTangentStream> : public TStructOpsTypeTraitsBase2<FVoxelMeshVertexTangentStream>
{
	enum
	{
		WithSerializer = true
	};
};


struct DYNAMICVOXELTERRAIN_API FVoxelMeshVertexTexCoordStream
{


private:
	TArray<uint8> Data;
	int32 ChannelCount;
	bool bIsHighPrecision;

public:
	FVoxelMeshVertexTexCoordStream(int32 InChannelCount = 1, bool bInShouldUseHighPrecision = false)
		: ChannelCount(InChannelCount), bIsHighPrecision(bInShouldUseHighPrecision)
	{
	}

	FORCEINLINE bool IsHighPrecision() const { return bIsHighPrecision; }
	FORCEINLINE int32 NumChannels() const
	{
		return ChannelCount;
	}

	FORCEINLINE int32 GetElementSize() const { return (bIsHighPrecision ? sizeof(FVector2D) : sizeof(FVector2DHalf)); } //size in bytes
	FORCEINLINE int32 GetStride() const { return GetElementSize() * ChannelCount; } //num of bytes of UV per vertex in data

	FORCEINLINE int32 Num() const
	{
		return Data.Num() / GetStride();
	}
	void SetNum(int32 NewNum, bool bAllowShrinking = true)
	{
		Data.SetNum(NewNum * GetStride(), bAllowShrinking);
	}
	void Reserve(int32 Number)
	{
		Data.Reserve(Number * GetStride());
	}
	void Empty(int32 Slack = 0)
	{
		Data.Empty(Slack * GetStride());
	}

	/* Prefer using SetNum and SetTexCoord when you have more than 1 UV
	Add appends a new element to the end of the array, but will only work if you cycle through each UV
	Example with 3 UVs :
	Add at channel 0
	Add at channel 1
	Add at channel 2
	Repeat
	If you skip any it'll crash UE
	(if you're here because it crashed, told you !)
	*/
	int32 Add(const FVector2D& InTexCoord, int32 ChannelId = 0)
	{
		int32 Index = Data.Num();
		checkf((Index / GetElementSize()) % ChannelCount == ChannelId, TEXT("[FVoxelMeshVertexTexCoordStream::Add] UVs have been added out of order, aborting..."));
		Data.AddZeroed(GetElementSize());

		if (bIsHighPrecision)
		{
			static const int32 ElementSize = sizeof(FVector2D);
			*((FVector2D*)&Data[Index]) = InTexCoord;
		}
		else
		{
			static const int32 ElementSize = sizeof(FVector2DHalf);
			*((FVector2DHalf*)&Data[Index]) = InTexCoord;
		}

		return Index / GetStride();
	}

	/* Add a UV at the end of the array
	Current UV Array must end with the last UV (meaning the previous vert must have had all of it's UVs registered)
	Given array must be of the same length as the number of UV channels
	*/
	int32 Add(const TArray<FVector2D>& InTexCoords)
	{
		const int oldNum = Data.Num();
		const int32 Index = Num();
		const int32 Stride = GetStride();
		checkf(InTexCoords.Num() == ChannelCount, TEXT("[FVoxelMeshVertexTexCoordStream::Add] Given array of UVs doesn't match UV channel count, aborting..."));
		checkf((oldNum / GetElementSize()) % ChannelCount == 0, TEXT("[FVoxelMeshVertexTexCoordStream::Add] Current array of UVs didn't end with the last UV channel, aborting..."));
		Data.AddZeroed(Stride);

		if (bIsHighPrecision)
		{
			static const int32 ElementSize = sizeof(FVector2D);

			for (int32 ChannelId = 0; ChannelId < ChannelCount; ChannelId++)
			{
				*((FVector2D*)&Data[(Index * Stride) + (ChannelId * ElementSize)]) = InTexCoords[ChannelId];
			}
		}
		else
		{
			static const int32 ElementSize = sizeof(FVector2DHalf);

			for (int32 ChannelId = 0; ChannelId < ChannelCount; ChannelId++)
			{
				*((FVector2DHalf*)&Data[(Index * Stride) + (ChannelId * ElementSize)]) = InTexCoords[ChannelId];
			}

		}

		return Index;
	}

	void Append(const FVoxelMeshVertexTexCoordStream& InOther)
	{
		checkf(InOther.bIsHighPrecision == bIsHighPrecision, TEXT("[FVoxelMeshVertexTexCoordStream::Append] Tried to merge two UV stream of different precision, aborting... (merge %s into %s)"),
			InOther.bIsHighPrecision ? TEXT("HighPrecision") : TEXT("LowPrecision"),
			bIsHighPrecision ? TEXT("HighPrecision") : TEXT("LowPrecision"));
		Data.Append(InOther.Data);
	}
	void FillIn(int32 StartIndex, const TArray<FVector2D>& InChannelData, int32 ChannelId = 0)
	{
		if (Num() < (StartIndex + InChannelData.Num()))
		{
			SetNum(StartIndex + InChannelData.Num());
		}

		for (int32 Index = 0; Index < InChannelData.Num(); Index++)
		{
			SetTexCoord(StartIndex + Index, InChannelData[Index], ChannelId);
		}
	}

	void SetTexCoord(int32 Index, const FVector2D& NewTexCoord, int32 ChannelId = 0)
	{
		if (bIsHighPrecision)
		{
			static const int32 ElementSize = sizeof(FVector2D);
			*((FVector2D*)&Data[(Index * ElementSize * ChannelCount) + (ChannelId * ElementSize)]) = NewTexCoord;
		}
		else
		{
			static const int32 ElementSize = sizeof(FVector2DHalf);
			*((FVector2DHalf*)&Data[(Index * ElementSize * ChannelCount) + (ChannelId * ElementSize)]) = NewTexCoord;
		}
	}
	const FVector2D GetTexCoord(int32 Index, int32 ChannelId = 0) const
	{
		if (bIsHighPrecision)
		{
			static const int32 ElementSize = sizeof(FVector2D);
			return *((FVector2D*)&Data[(Index * ElementSize * ChannelCount) + (ChannelId * ElementSize)]);
		}
		else
		{
			static const int32 ElementSize = sizeof(FVector2DHalf);
			return *((FVector2DHalf*)&Data[(Index * ElementSize * ChannelCount) + (ChannelId * ElementSize)]);
		}
	}

	const uint8* GetData() const { return Data.GetData(); }
	TArray<uint8>&& TakeData()&& { return MoveTemp(Data); }
	// 	const TArray<TArray<FVector2D>> GetCopy() const
	// 	{
	// 		TArray<FVector> OutData;
	// 		OutData.SetNum(Num());
	// 		FMemory::Memcpy(OutData.GetData(), Data.GetData(), Data.Num());
	// 		return OutData;
	// 	}

	bool Serialize(FArchive& Ar)
	{
		Ar << bIsHighPrecision;
		Ar << ChannelCount;
		Ar << Data;
		return true;
	}
};

template<> struct TStructOpsTypeTraits<FVoxelMeshVertexTexCoordStream> : public TStructOpsTypeTraitsBase2<FVoxelMeshVertexTexCoordStream>
{
	enum
	{
		WithSerializer = true
	};
};


struct DYNAMICVOXELTERRAIN_API FVoxelMeshVertexColorStream
{


private:
	TArray<uint8> Data;

public:
	FVoxelMeshVertexColorStream() { }

	FORCEINLINE int32 GetStride() const { return sizeof(FColor); }

	FORCEINLINE int32 Num() const
	{
		return Data.Num() / GetStride();
	}
	void SetNum(int32 NewNum, bool bAllowShrinking = true)
	{
		Data.SetNum(NewNum * GetStride(), bAllowShrinking);
	}
	void Reserve(int32 Number)
	{
		Data.Reserve(Number * GetStride());
	}
	void Empty(int32 Slack = 0)
	{
		Data.Empty(Slack * GetStride());
	}

	int32 Add(const FColor& InColor)
	{
		int32 Index = Data.Num();
		Data.AddUninitialized(sizeof(FColor));
		*((FColor*)&Data[Index]) = InColor;
		return Index / sizeof(FColor);
	}
	void Append(const FVoxelMeshVertexColorStream& InOther)
	{
		Data.Append(InOther.Data);
	}
	void Append(const TArray<FColor>& InColors)
	{
		int32 StartIndex = Data.Num();
		Data.SetNum(StartIndex + sizeof(FColor) * InColors.Num());
		FMemory::Memcpy(Data.GetData() + StartIndex, InColors.GetData(), InColors.Num() * sizeof(FColor));
	}
	void Append(const TArray<FLinearColor>& InColors)
	{
		int32 StartIndex = Num();
		SetNum(Num() + InColors.Num());
		for (int32 Index = 0; Index < InColors.Num(); Index++)
		{
			SetColor(StartIndex + Index, InColors[Index].ToFColor(true));
		}
	}

	void SetColor(int32 Index, const FColor& NewColor)
	{
		*((FColor*)&Data[Index * sizeof(FColor)]) = NewColor;
	}
	const FColor& GetColor(int32 Index) const
	{
		return *((FColor*)&Data[Index * sizeof(FColor)]);
	}

	const uint8* GetData() const { return Data.GetData(); }
	TArray<uint8>&& TakeData()&& { return MoveTemp(Data); }
	// 	const TArray<TArray<FVector2D>> GetCopy() const
	// 	{
	// 		TArray<FVector> OutData;
	// 		OutData.SetNum(Num());
	// 		FMemory::Memcpy(OutData.GetData(), Data.GetData(), Data.Num());
	// 		return OutData;
	// 	}

	bool Serialize(FArchive& Ar)
	{
		Ar << Data;
		return true;
	}
};

template<> struct TStructOpsTypeTraits<FVoxelMeshVertexColorStream> : public TStructOpsTypeTraitsBase2<FVoxelMeshVertexColorStream>
{
	enum
	{
		WithSerializer = true
	};
};


struct DYNAMICVOXELTERRAIN_API FVoxelMeshTriangleStream
{


private:
	TArray<uint8> Data;
	uint8 Stride;

public:
	FVoxelMeshTriangleStream()
		: Stride(sizeof(uint32))
	{

	}

	bool IsHighPrecision() const { return true; }

	FORCEINLINE int32 GetStride() const { return Stride; }

	FORCEINLINE int32 Num() const
	{
		return Data.Num() / GetStride();
	}
	int32 NumTriangles() const
	{
		return (Num() / 3);
	}
	void SetNum(int32 NewNum, bool bAllowShrinking = true)
	{
		Data.SetNum(NewNum * GetStride(), bAllowShrinking);
	}
	void Reserve(int32 Number)
	{
		Data.Reserve(Number * GetStride());
	}
	void Empty(int32 Slack = 0)
	{
		Data.Empty(Slack * GetStride());
	}

	int32 Add(uint32 NewIndex)
	{
		int32 Index = Data.Num();
		Data.AddUninitialized(Stride);

		*((uint32*)&Data[Index]) = NewIndex;
		return Index / Stride;
	}
	int32 AddTriangle(uint32 NewIndexA, uint32 NewIndexB, uint32 NewIndexC)
	{
		int32 Index = Data.Num();
		Data.AddUninitialized(Stride * 3);

		*((uint32*)&Data[Index]) = NewIndexA;
		Index += Stride;
		*((uint32*)&Data[Index]) = NewIndexB;
		Index += Stride;
		*((uint32*)&Data[Index]) = NewIndexC;
		return Index / Stride;
	}
	void Append(const FVoxelMeshTriangleStream& InOther)
	{
		Data.Append(InOther.Data);
	}
	void Append(const TArray<uint32>& InTriangles)
	{
		int32 StartIndex = Num();

		Data.SetNum(StartIndex + sizeof(int32) * InTriangles.Num());
		FMemory::Memcpy(Data.GetData() + StartIndex, InTriangles.GetData(), InTriangles.Num() * sizeof(int32));
	}
	void SetVertexIndex(int32 Index, uint32 NewIndex)
	{
		*((uint32*)&Data[Index * Stride]) = NewIndex;
	}
	uint32 GetVertexIndex(int32 Index) const
	{
		return *((uint32*)&Data[Index * Stride]);
	}

	const uint8* GetData() const { return Data.GetData(); }
	TArray<uint8>&& TakeData()&& { return MoveTemp(Data); }
	// 	const TArray<TArray<FVector2D>> GetCopy() const
	// 	{
	// 		TArray<FVector> OutData;
	// 		OutData.SetNum(Num());
	// 		FMemory::Memcpy(OutData.GetData(), Data.GetData(), Data.Num());
	// 		return OutData;
	// 	}

	bool Serialize(FArchive& Ar)
	{
		Ar << Stride;
		Ar << Data;
		return true;
	}
};

template<> struct TStructOpsTypeTraits<FVoxelMeshTriangleStream> : public TStructOpsTypeTraitsBase2<FVoxelMeshTriangleStream>
{
	enum
	{
		WithSerializer = true
	};
};

struct DYNAMICVOXELTERRAIN_API FVoxelMeshRenderUpdateData
{
	FVoxelMeshVertexPositionStream Positions;
	FVoxelMeshVertexTangentStream Tangents;
	FVoxelMeshVertexTexCoordStream TexCoords;
	FVoxelMeshVertexColorStream Colors;
	FVoxelMeshTriangleStream Triangles;
	//Dynamic sections require to be recreated fo
};

using FVoxelMeshDataSharedPtr = TSharedPtr<FVoxelMeshData, ESPMode::ThreadSafe>;