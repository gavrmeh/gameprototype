#pragma once
#include "RenderResource.h"
#include "Rendering/VoxelMeshData.h"
#include "RHICommandList.h"
//Buffers implementation from RMC
//Copyright(c) 2016 - 2020 TriAxis Games L.L.C.

class FVoxelMeshBufferUpdateData : public FResourceArrayInterface
{
	int32 ElementStride;
	int32 NumElements;
	TArray<uint8> Data;

public:


	FVoxelMeshBufferUpdateData(FVoxelMeshVertexPositionStream&& InPositions)
		: ElementStride(InPositions.GetStride())
		, NumElements(InPositions.Num())
		, Data(MoveTemp(InPositions).TakeData())
	{

	}

	FVoxelMeshBufferUpdateData(FVoxelMeshVertexTangentStream&& InTangents)
		: ElementStride(InTangents.GetStride())
		, NumElements(InTangents.Num())
		, Data(MoveTemp(InTangents).TakeData())
	{

	}

	FVoxelMeshBufferUpdateData(FVoxelMeshVertexTexCoordStream&& InTexCoords)
		: ElementStride(InTexCoords.GetStride())
		, NumElements(InTexCoords.Num())
		, Data(MoveTemp(InTexCoords).TakeData())
	{

	}

	FVoxelMeshBufferUpdateData(FVoxelMeshVertexColorStream&& InColors)
		: ElementStride(InColors.GetStride())
		, NumElements(InColors.Num())
		, Data(MoveTemp(InColors).TakeData())
	{

	}

	FVoxelMeshBufferUpdateData(FVoxelMeshTriangleStream&& InTriangles)
		: ElementStride(InTriangles.GetStride())
		, NumElements(InTriangles.Num())
		, Data(MoveTemp(InTriangles).TakeData())
	{

	}

	virtual ~FVoxelMeshBufferUpdateData() {}


	int32 GetStride() const { return ElementStride; }
	int32 GetNumElements() const { return NumElements; }

	const void* GetResourceData() const override
	{
		return reinterpret_cast<const void*>(Data.GetData());
	}
	uint32 GetResourceDataSize() const override
	{
		return Data.Num();
	}
	void Discard() override
	{
		Data.Empty();
	}
	bool IsStatic() const override
	{
		return false;
	}
	bool GetAllowCPUAccess() const override
	{
		return true;
	}
	void SetAllowCPUAccess(bool bInNeedsCPUAccess) override
	{
	}

};

class DYNAMICVOXELTERRAIN_API FVoxelMeshSectionUpdateData
{
public:

	FVoxelMeshBufferUpdateData Positions;
	FVoxelMeshBufferUpdateData Tangents;
	FVoxelMeshBufferUpdateData TexCoords;
	FVoxelMeshBufferUpdateData Colors;

	FVoxelMeshBufferUpdateData Triangles;




	FVertexBufferRHIRef PositionsBuffer;
	FVertexBufferRHIRef TangentsBuffer;
	FVertexBufferRHIRef TexCoordsBuffer;
	FVertexBufferRHIRef ColorsBuffer;

	FIndexBufferRHIRef TrianglesBuffer;

	const uint8 NumTexCoordChannels;


	bool bBuffersCreated;


	FVoxelMeshSectionUpdateData(FVoxelMeshRenderUpdateData&& InMesh)
		: Positions(MoveTemp(InMesh.Positions))
		, Tangents(MoveTemp(InMesh.Tangents))
		, TexCoords(MoveTemp(InMesh.TexCoords))
		, Colors(MoveTemp(InMesh.Colors))
		, Triangles(MoveTemp(InMesh.Triangles))
		, NumTexCoordChannels(InMesh.TexCoords.NumChannels())
		, bBuffersCreated(false)
	{

	}

	template<bool bIsInRenderThread>
	void CreateRHIBuffers(bool bShouldUseDynamicBuffers);

};

struct DYNAMICVOXELTERRAIN_API FVoxelMeshSectionCreateData
{
	FVoxelMeshSectionCreateData(FVoxelMeshRenderUpdateData&& InMesh, bool bShouldCreateDynamicBuffers)
	: bCreateDynamicSection(bShouldCreateDynamicBuffers)
	, UpdateData(MoveTemp(InMesh))
	{
	
	};
	FVoxelMeshSectionUpdateData UpdateData;
	bool bCreateDynamicSection = false;
};




/** Single vertex buffer to hold one vertex stream within a section */
class FVoxelMeshVertexBuffer : public FVertexBuffer
{
protected:

	/** Should this buffer by flagged as dynamic */
	const bool bIsDynamicBuffer;

	/** Size of a single vertex */
	int32 VertexSize;

	/** The number of vertices this buffer is currently allocated to hold */
	int32 NumVertices;

	/** Shader Resource View for this buffer */
	FShaderResourceViewRHIRef ShaderResourceView;

public:

	FVoxelMeshVertexBuffer(bool bInIsDynamicBuffer, int32 DefaultVertexSize);

	~FVoxelMeshVertexBuffer() {}

	virtual void InitRHI() override;
	void ReleaseRHI() override;

	/** Gets the size of the vertex */
	FORCEINLINE int32 Stride() const { return VertexSize; }

	/** Get the size of the vertex buffer */
	FORCEINLINE int32 Num() const { return NumVertices; }

	/** Gets the full allocated size of the buffer (Equal to VertexSize * NumVertices) */
	FORCEINLINE int32 GetBufferSize() const { return NumVertices * VertexSize; }

	/** Gets the size of a single piece of data within an element */
	virtual int32 GetElementDatumSize() const = 0;

	/** Gets the format of the element, needed for creation of SRV's */
	virtual EPixelFormat GetElementFormat() const = 0;

	/** Binds the vertex buffer to the factory data type */
	virtual void Bind(FLocalVertexFactory::FDataType& DataType) = 0;




public:
	
	template<bool bIsInRenderThread>
	static FVertexBufferRHIRef CreateRHIBuffer(FRHIResourceCreateInfo& CreateInfo, uint32 SizeInBytes, bool bDynamicBuffer)
	{
		const int32 Flags = (bDynamicBuffer ? BUF_Dynamic : BUF_Static) | BUF_ShaderResource;
		CreateInfo.bWithoutNativeResource = SizeInBytes <= 0;
		if (bIsInRenderThread)
		{
			return RHICreateVertexBuffer(SizeInBytes, Flags, CreateInfo);
		}
		else
		{
			return RHIAsyncCreateVertexBuffer(SizeInBytes, Flags, CreateInfo);
		}
		return nullptr;
	}

	template<bool bIsInRenderThread>
	static FVertexBufferRHIRef CreateRHIBuffer(FVoxelMeshBufferUpdateData& InStream, bool bDynamicBuffer)
	{
		const uint32 SizeInBytes = InStream.GetResourceDataSize();

		FRHIResourceCreateInfo CreateInfo(TEXT("VoxelMeshComponent"), &InStream);

		return CreateRHIBuffer<bIsInRenderThread>(CreateInfo, SizeInBytes, bDynamicBuffer);
	}


	template <uint32 MaxNumUpdates>
	void UpdateRHIFromExisting(FRHIVertexBuffer* IntermediateBuffer, TRHIResourceUpdateBatcher<MaxNumUpdates>& Batcher)
	{
		check(VertexBufferRHI && IntermediateBuffer);

		Batcher.QueueUpdateRequest(VertexBufferRHI, IntermediateBuffer);

		if (ShaderResourceView)
		{
			Batcher.QueueUpdateRequest(ShaderResourceView, VertexBufferRHI, GetElementDatumSize(), GetElementFormat());
		}
	}

	void InitRHIFromExisting(const FVertexBufferRHIRef& InVertexBufferRHI)
	{
		InitResource();
		check(InVertexBufferRHI);

		VertexBufferRHI = InVertexBufferRHI;
		if (VertexBufferRHI.IsValid() && RHISupportsManualVertexFetch(GMaxRHIShaderPlatform))
		{
			ShaderResourceView = RHICreateShaderResourceView(VertexBufferRHI, GetElementDatumSize(), GetElementFormat());
		}
	}


};

class FVoxelMeshPositionVertexBuffer : public FVoxelMeshVertexBuffer
{
public:
	FVoxelMeshPositionVertexBuffer(bool bInIsDynamicBuffer)
		: FVoxelMeshVertexBuffer(bInIsDynamicBuffer, sizeof(FVector))
	{

	}

	void InitFromArray(const TArray<FVector>& InData)
	{
		
	}
	
	virtual FString GetFriendlyName() const override { return TEXT("FVoxelMeshPositionVertexBuffer"); }

	virtual int32 GetElementDatumSize() const override { return 4; }
	virtual EPixelFormat GetElementFormat() const override { return PF_R32_FLOAT; }

	virtual void Bind(FLocalVertexFactory::FDataType& DataType) override
	{
		DataType.PositionComponent = FVertexStreamComponent(this, 0, 12, VET_Float3);
		DataType.PositionComponentSRV = ShaderResourceView;
	}

	template <uint32 MaxNumUpdates>
	void UpdateRHIFromExisting(FRHIVertexBuffer* IntermediateBuffer, int32 NumElements, TRHIResourceUpdateBatcher<MaxNumUpdates>& Batcher)
	{
		if (VertexBufferRHI && IntermediateBuffer)
		{
			VertexSize = sizeof(FVector);
			NumVertices = NumElements;

			FVoxelMeshVertexBuffer::UpdateRHIFromExisting<MaxNumUpdates>(IntermediateBuffer, Batcher);
		}
	}

	void InitRHIFromExisting(const FVertexBufferRHIRef& InVertexBufferRHI, int32 NumElements)
	{
		VertexSize = sizeof(FVector);
		NumVertices = NumElements;

		FVoxelMeshVertexBuffer::InitRHIFromExisting(InVertexBufferRHI);
	}
};

class FVoxelMeshTangentsVertexBuffer : public FVoxelMeshVertexBuffer
{
	static constexpr int32 CalculateStride()
	{
		return (sizeof(FPackedNormal)) * 2;
	}

private:


public:
	FVoxelMeshTangentsVertexBuffer(bool bInIsDynamicBuffer)
		: FVoxelMeshVertexBuffer(bInIsDynamicBuffer, CalculateStride())
	{

	}

	virtual FString GetFriendlyName() const override { return TEXT("FVoxelMeshTangentsVertexBuffer"); }

	virtual int32 GetElementDatumSize() const override { return sizeof(FPackedNormal); }
	virtual EPixelFormat GetElementFormat() const override { return PF_R8G8B8A8_SNORM; }

	virtual void Bind(FLocalVertexFactory::FDataType& DataType) override
	{
		uint32 TangentSizeInBytes = 0;
		uint32 TangentXOffset = 0;
		uint32 TangentZOffset = 0;
		EVertexElementType TangentElementType = VET_None;

		TangentElementType = VET_PackedNormal;
		TangentSizeInBytes = sizeof(FPackedNormal) * 2;
		TangentXOffset = 0;
		TangentZOffset = sizeof(FPackedNormal);

		DataType.TangentBasisComponents[0] = FVertexStreamComponent(this, TangentXOffset, TangentSizeInBytes, TangentElementType, EVertexStreamUsage::ManualFetch);
		DataType.TangentBasisComponents[1] = FVertexStreamComponent(this, TangentZOffset, TangentSizeInBytes, TangentElementType, EVertexStreamUsage::ManualFetch);
		DataType.TangentsSRV = ShaderResourceView;
	}

	template <uint32 MaxNumUpdates>
	void UpdateRHIFromExisting(FRHIVertexBuffer* IntermediateBuffer, int32 NumElements, bool bShouldUseHighPrecision, TRHIResourceUpdateBatcher<MaxNumUpdates>& Batcher)
	{
		if (VertexBufferRHI && IntermediateBuffer)
		{
			bUseHighPrecision = bShouldUseHighPrecision;
			VertexSize = CalculateStride(bShouldUseHighPrecision);
			NumVertices = NumElements;

			FVoxelMeshVertexBuffer::UpdateRHIFromExisting<MaxNumUpdates>(IntermediateBuffer, Batcher);
		}
	}

	void InitRHIFromExisting(const FVertexBufferRHIRef& InVertexBufferRHI, int32 NumElements)
	{
		VertexSize = CalculateStride();
		NumVertices = NumElements;

		FVoxelMeshVertexBuffer::InitRHIFromExisting(InVertexBufferRHI);
	}

};

class FVoxelMeshTexCoordsVertexBuffer : public FVoxelMeshVertexBuffer
{
	static constexpr int32 CalculateStride(int32 InNumUVs)
	{
		return sizeof(FVector2D) * InNumUVs;
	}

private:
	/** Whether this uv buffer is using high precision uvs */

	/** Num UV's in use */
	int32 NumUVs;

public:
	FVoxelMeshTexCoordsVertexBuffer(bool bInIsDynamicBuffer)
		: FVoxelMeshVertexBuffer(bInIsDynamicBuffer, CalculateStride(1))
		, NumUVs(1)
	{

	}

	virtual FString GetFriendlyName() const override { return TEXT("FVoxelMeshUVsVertexBuffer"); }

	virtual int32 GetElementDatumSize() const override { return sizeof(FVector2D); }
	virtual EPixelFormat GetElementFormat() const override { return PF_G32R32F; }

	virtual void Bind(FLocalVertexFactory::FDataType& DataType) override
	{
		DataType.TextureCoordinates.Empty();
		DataType.NumTexCoords = NumUVs;


		EVertexElementType UVDoubleWideVertexElementType = VET_None;
		EVertexElementType UVVertexElementType = VET_None;
		uint32 UVSizeInBytes = 0;
		UVSizeInBytes = sizeof(FVector2D);
		UVDoubleWideVertexElementType = VET_Float4;
		UVVertexElementType = VET_Float2;
		uint32 UVStride = UVSizeInBytes * NumUVs;

		int32 UVIndex;
		for (UVIndex = 0; UVIndex < NumUVs - 1; UVIndex += 2)
		{
			DataType.TextureCoordinates.Add(FVertexStreamComponent(this, UVSizeInBytes * UVIndex, UVStride, UVDoubleWideVertexElementType, EVertexStreamUsage::ManualFetch));
		}

		// possible last UV channel if we have an odd number
		if (UVIndex < NumUVs)
		{
			DataType.TextureCoordinates.Add(FVertexStreamComponent(this, UVSizeInBytes * UVIndex, UVStride, UVVertexElementType, EVertexStreamUsage::ManualFetch));
		}

		DataType.TextureCoordinatesSRV = ShaderResourceView;
	}

	template <uint32 MaxNumUpdates>
	void UpdateRHIFromExisting(FRHIVertexBuffer* IntermediateBuffer, int32 NumElements, int32 NumChannels, TRHIResourceUpdateBatcher<MaxNumUpdates>& Batcher)
	{
		if (VertexBufferRHI && IntermediateBuffer)
		{
			NumUVs = NumChannels;
			VertexSize = CalculateStride(NumChannels);
			NumVertices = NumElements;

			FVoxelMeshVertexBuffer::UpdateRHIFromExisting<MaxNumUpdates>(IntermediateBuffer, Batcher);
		}
	}

	void InitRHIFromExisting(const FVertexBufferRHIRef& InVertexBufferRHI, int32 NumElements, int32 NumChannels)
	{
		NumUVs = NumChannels;
		VertexSize = CalculateStride(NumChannels);
		NumVertices = NumElements;

		FVoxelMeshVertexBuffer::InitRHIFromExisting(InVertexBufferRHI);
	}

};

class FVoxelMeshColorVertexBuffer : public FVoxelMeshVertexBuffer
{
public:
	FVoxelMeshColorVertexBuffer(bool bInIsDynamicBuffer)
		: FVoxelMeshVertexBuffer(bInIsDynamicBuffer, sizeof(FColor))
	{

	}

	virtual FString GetFriendlyName() const override { return TEXT("FVoxelMeshColorVertexBuffer"); }

	virtual int32 GetElementDatumSize() const override { return sizeof(FColor); }
	virtual EPixelFormat GetElementFormat() const override { return PF_R8G8B8A8; }

	virtual void Bind(FLocalVertexFactory::FDataType& DataType) override
	{
		DataType.ColorComponent = FVertexStreamComponent(this, 0, 4, EVertexElementType::VET_Color, EVertexStreamUsage::ManualFetch);
		DataType.ColorComponentsSRV = ShaderResourceView;
	}

	template <uint32 MaxNumUpdates>
	void UpdateRHIFromExisting(FRHIVertexBuffer* IntermediateBuffer, int32 NumElements, TRHIResourceUpdateBatcher<MaxNumUpdates>& Batcher)
	{
		if (VertexBufferRHI && IntermediateBuffer)
		{
			VertexSize = sizeof(FColor);
			NumVertices = NumElements;

			FVoxelMeshVertexBuffer::UpdateRHIFromExisting<MaxNumUpdates>(IntermediateBuffer, Batcher);
		}
	}

	void InitRHIFromExisting(const FVertexBufferRHIRef& InVertexBufferRHI, int32 NumElements)
	{
		VertexSize = sizeof(FColor);
		NumVertices = NumElements;

		FVoxelMeshVertexBuffer::InitRHIFromExisting(InVertexBufferRHI);
	}

};





/** Index Buffer */
class FVoxelMeshIndexBuffer : public FIndexBuffer
{
	static constexpr int32 CalculateStride()
	{
		return sizeof(int32);
	}

private:
	/** Should this buffer by flagged as dynamic */
	bool bIsDynamicBuffer;

	/* The size of a single index*/
	int32 IndexSize;

	/* The number of indices this buffer is currently allocated to hold */
	int32 NumIndices;

public:

	FVoxelMeshIndexBuffer(bool bInIsDynamicBuffer);

	virtual FString GetFriendlyName() const override { return TEXT("FVoxelMeshIndexBuffer"); }

	virtual void InitRHI() override;

	/* Get the size of the index buffer */
	int32 Num() const { return NumIndices; }

	/** Gets the full allocated size of the buffer (Equal to IndexSize * NumIndices) */
	int32 GetBufferSize() const { return NumIndices * IndexSize; }

	/* Set the data for the index buffer */
	//void SetData(bool bInUse32BitIndices, int32 NewIndexCount, const uint8* InData);



	template<bool bIsInRenderThread>
	static FIndexBufferRHIRef CreateRHIBuffer(FRHIResourceCreateInfo& CreateInfo, uint32 IndexSize, uint32 SizeInBytes, bool bDynamicBuffer)
	{
		const int32 Flags = (bDynamicBuffer ? BUF_Dynamic : BUF_Static) | BUF_ShaderResource;
		CreateInfo.bWithoutNativeResource = SizeInBytes <= 0;
		if (bIsInRenderThread)
		{
			return RHICreateIndexBuffer(IndexSize, SizeInBytes, Flags, CreateInfo);
		}
		else
		{
			return RHIAsyncCreateIndexBuffer(IndexSize, SizeInBytes, Flags, CreateInfo);
		}
		return nullptr;

	}

	template<bool bIsInRenderThread>
	static FIndexBufferRHIRef CreateRHIBuffer(FVoxelMeshBufferUpdateData& InStream, bool bDynamicBuffer)
	{
		const uint32 SizeInBytes = InStream.GetResourceDataSize();

		FRHIResourceCreateInfo CreateInfo(TEXT("VoxelMeshComponent"), &InStream);

		return CreateRHIBuffer<bIsInRenderThread>(CreateInfo, InStream.GetStride(), SizeInBytes, bDynamicBuffer);
	}


	template <uint32 MaxNumUpdates>
	void UpdateRHIFromExisting(FRHIIndexBuffer* IntermediateBuffer, int32 NumElements, TRHIResourceUpdateBatcher<MaxNumUpdates>& Batcher)
	{
		if (IndexBufferRHI && IntermediateBuffer)
		{
			IndexSize = CalculateStride(bShouldUseHighPrecision);
			NumIndices = NumElements;

			Batcher.QueueUpdateRequest(IndexBufferRHI, IntermediateBuffer);
		}
	}

	void InitRHIFromExisting(const FIndexBufferRHIRef& InIndexBufferRHI, int32 NumElements)
	{
		InitResource();

		IndexSize = CalculateStride();
		NumIndices = NumElements;

		if (InIndexBufferRHI)
		{
			IndexBufferRHI = InIndexBufferRHI;
		}
		else
		{
			FRHIResourceCreateInfo CreateInfo(TEXT("VoxelMeshComponent"));
			IndexBufferRHI = CreateRHIBuffer<true>(CreateInfo, IndexSize, 0, bIsDynamicBuffer);
		}
	}

};

/** Vertex Factory */
class FVoxelMeshVertexFactory : public FLocalVertexFactory
{
public:

	FVoxelMeshVertexFactory(ERHIFeatureLevel::Type InFeatureLevel);

	/** Init function that can be called on any thread, and will do the right thing (enqueue command if called on main thread) */
	void Init(FLocalVertexFactory::FDataType VertexStructure);

	/* Gets the section visibility for static sections */
	//virtual uint64 GetStaticBatchElementVisibility(const class FSceneView& View, const struct FMeshBatch* Batch, const void* InViewCustomData = nullptr) const override;

private:
	/* Interface to the parent section for checking visibility.*/
	//FVoxelMeshSectionProxy* SectionParent;
};

template<bool bIsInRenderThread>
void FVoxelMeshSectionUpdateData::CreateRHIBuffers(bool bShouldUseDynamicBuffers)
{
	if (!bBuffersCreated)
	{

		PositionsBuffer = FVoxelMeshVertexBuffer::CreateRHIBuffer<bIsInRenderThread>(Positions, bShouldUseDynamicBuffers);
		TangentsBuffer = FVoxelMeshVertexBuffer::CreateRHIBuffer<bIsInRenderThread>(Tangents, bShouldUseDynamicBuffers);
		TexCoordsBuffer = FVoxelMeshVertexBuffer::CreateRHIBuffer<bIsInRenderThread>(TexCoords, bShouldUseDynamicBuffers);
		ColorsBuffer = FVoxelMeshVertexBuffer::CreateRHIBuffer<bIsInRenderThread>(Colors, bShouldUseDynamicBuffers);

		TrianglesBuffer = FVoxelMeshIndexBuffer::CreateRHIBuffer<bIsInRenderThread>(Triangles, bShouldUseDynamicBuffers);

		bBuffersCreated = true;
	}
}
