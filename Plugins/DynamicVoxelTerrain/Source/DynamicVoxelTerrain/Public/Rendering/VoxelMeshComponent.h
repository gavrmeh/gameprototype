#pragma once

#include "CoreMinimal.h"
#include "Rendering/VoxelMeshData.h"
#include "Components/MeshComponent.h"
#include "Materials/MaterialInterface.h"
#include "UObject/ConstructorHelpers.h"
#include "VoxelMeshComponent.generated.h"


UCLASS()
class DYNAMICVOXELTERRAIN_API UVoxelMeshComponent : public UMeshComponent
{
	GENERATED_BODY()
public:
	
	UVoxelMeshComponent();

	float Test;

	void CreateSection(uint16 SectionIndex, const FVoxelMeshData& MeshData);
	void RemoveSection(uint16 SectionIndex);
	void UpdateSection(uint16 SectionIndex, const FVoxelMeshData& MeshData);

	void Update(const FVoxelMeshDataSharedPtr& Data);

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual FPrimitiveSceneProxy* CreateSceneProxy() override final;

	virtual void GetUsedMaterials(TArray<UMaterialInterface*>& Output, bool bGetDebugMaterials) const override
	{
		Output.Push(TestMaterial);
	};
	UMaterialInterface* TestMaterial = nullptr;
private:
};