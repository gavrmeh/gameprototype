#include "Rendering/VoxelMeshComponent.h"
#include "Rendering/VoxelMeshRendering.h"
UVoxelMeshComponent::UVoxelMeshComponent()
{
	Bounds = FBoxSphereBounds(FBox({0,0,0},{1,1,1}), FSphere(FVector(0.5), 0.5));
	static ConstructorHelpers::FObjectFinder<UMaterialInterface> Material(TEXT("/Game/StarterContent/Materials/M_Concrete_Tiles.M_Concrete_Tiles"));
	if (Material.Succeeded())
	{
		TestMaterial = Material.Object;
	}
	SetMaterial(0, TestMaterial);
	UE_LOG(LogTemp, Display, TEXT("Called UVoxelMeshComponent()"));
}

void UVoxelMeshComponent::CreateSection(uint16 SectionIndex, const FVoxelMeshData& MeshData)
{
	UE_LOG(LogTemp, Display, TEXT("VoxelMeshComponent: Called CreateSection()"));
	FVoxelMeshSceneProxy* Proxy = reinterpret_cast<FVoxelMeshSceneProxy*>(SceneProxy);
	FVoxelMeshRenderUpdateData UpdateData;
	UpdateData.Positions.Append(MeshData.Positions);
	UpdateData.Tangents.Append(MeshData.Tangents);
	UpdateData.Colors.Append(MeshData.Colors);
	UpdateData.TexCoords.FillIn(0, MeshData.UVs, 0);
	UpdateData.Triangles.Append(MeshData.Indecies);
	ENQUEUE_RENDER_COMMAND(CreateVoxelMeshSection)(
	[Proxy, SectionIndex, UpdateData](FRHICommandListImmediate& CmdList)mutable
	{
		Proxy->CreateSection_RenderThread(SectionIndex, MoveTemp(UpdateData));
	});
	
}

void UVoxelMeshComponent::RemoveSection(uint16 SectionIndex)
{
}

void UVoxelMeshComponent::UpdateSection(uint16 SectionIndex, const FVoxelMeshData& MeshData)
{
}

void UVoxelMeshComponent::Update(const FVoxelMeshDataSharedPtr& Data)
{
}

void UVoxelMeshComponent::BeginPlay()
{
	Super::BeginPlay();
	Test = 0;
	FVoxelMeshData SectionData;
	SectionData.Positions.Push({ -100,-100,0 });
	SectionData.Positions.Push({ 100,-100,0 });
	SectionData.Positions.Push({ 0,100,0 });
	SectionData.Normals.Push({ -100,-100,0 });
	SectionData.Normals.Push({ 100,-100,0 });
	SectionData.Normals.Push({ 0,100,0 });
	SectionData.Colors.Push({ 1,0,0 });
	SectionData.Colors.Push({ 1,0,0 });
	SectionData.Colors.Push({ 1,0,0 });
	SectionData.UVs.Push({ 0,0 });
	SectionData.UVs.Push({ 0,1 });
	SectionData.UVs.Push({ 1,0 });
	SectionData.Tangents.Push({ {1,0,0},{0,0,1} });
	SectionData.Tangents.Push({ {1,0,0},{0,0,1} });
	SectionData.Tangents.Push({ {1,0,0},{0,0,1} });
	SectionData.Indecies.Push(0);
	SectionData.Indecies.Push(2);
	SectionData.Indecies.Push(1);
	UE_LOG(LogTemp, Display, TEXT("VoxelMeshComponent: Called BeginPlay()"));
	CreateSection(0, SectionData);
}

void UVoxelMeshComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
}

FPrimitiveSceneProxy* UVoxelMeshComponent::CreateSceneProxy()
{
	FVoxelMeshSceneProxy* Proxy = new FVoxelMeshSceneProxy(this);
	return Proxy;
}

