#include "Rendering/VoxelMeshBuffers.h"

FVoxelMeshVertexBuffer::FVoxelMeshVertexBuffer(bool bInIsDynamicBuffer, int32 DefaultVertexSize)
	: bIsDynamicBuffer(bInIsDynamicBuffer)
	, VertexSize(DefaultVertexSize)
	, NumVertices(0)
	, ShaderResourceView(nullptr)
{
}


void FVoxelMeshVertexBuffer::InitRHI()
{
}

void FVoxelMeshVertexBuffer::ReleaseRHI()
{
	ShaderResourceView.SafeRelease();
	FVertexBuffer::ReleaseRHI();
}



FVoxelMeshIndexBuffer::FVoxelMeshIndexBuffer(bool bInIsDynamicBuffer)
	: bIsDynamicBuffer(bInIsDynamicBuffer)
	, IndexSize(CalculateStride())
	, NumIndices(0)
{
}

void FVoxelMeshIndexBuffer::InitRHI()
{
}




FVoxelMeshVertexFactory::FVoxelMeshVertexFactory(ERHIFeatureLevel::Type InFeatureLevel)
	: FLocalVertexFactory(InFeatureLevel, "FVoxelMeshVertexFactory")
{
}

/** Init function that can be called on any thread, and will do the right thing (enqueue command if called on main thread) */
void FVoxelMeshVertexFactory::Init(FLocalVertexFactory::FDataType VertexStructure)
{
	if (IsInRenderingThread())
	{
		SetData(VertexStructure);
	}
	else
	{
		// Send the command to the render thread
		ENQUEUE_RENDER_COMMAND(InitVoxelMeshVertexFactory)(
			[this, VertexStructure](FRHICommandListImmediate& RHICmdList)
			{
				Init(VertexStructure);
			}
		);
	}
}