#include "Rendering/VoxelMeshRendering.h"
#include "Rendering/VoxelMeshComponent.h"
#include "CoreGlobals.h"
FVoxelMeshSectionData::FVoxelMeshSectionData(ERHIFeatureLevel::Type FeatureLevel, const FVoxelMeshSectionCreateData& MeshData)
: Buffers(MeshData)
, VertexFactory(FLocalVertexFactory(FeatureLevel, "VoxelMeshSectionVF"))
{
	check(IsInRenderingThread());
	FLocalVertexFactory::FDataType Data;
	Buffers.PositionsBuffer.Bind(Data);
	Buffers.TangentsBuffer.Bind(Data);
	Buffers.TexCoordsBuffer.Bind(Data);
	Buffers.ColorsBuffer.Bind(Data);
	VertexFactory.SetData(Data);
	InitOrUpdateResource(&VertexFactory);
	UE_LOG(LogTemp, Display, TEXT("VoxelMeshProxy: Frame %d Initialized resource."), GFrameCounter);
}

FVoxelMeshSectionData::~FVoxelMeshSectionData()
{
	VertexFactory.ReleaseResource();
}

void FVoxelMeshSectionData::Update(const FVoxelMeshSectionUpdateData& MeshData)
{
}

FVoxelMeshBuffers::FVoxelMeshBuffers(const FVoxelMeshSectionCreateData& InData)
: PositionsBuffer(InData.bCreateDynamicSection)
, TangentsBuffer(InData.bCreateDynamicSection)
, TexCoordsBuffer(InData.bCreateDynamicSection)
, ColorsBuffer(InData.bCreateDynamicSection)
, IndexBuffer(InData.bCreateDynamicSection)
, bDynamicBuffers(InData.bCreateDynamicSection)
{
	UE_LOG(LogTemp, Display, TEXT("VoxelMeshProxy: Frame %d Initialized buffers."), GFrameCounter);
	check(InData.UpdateData.bBuffersCreated)
	PositionsBuffer.InitRHIFromExisting(InData.UpdateData.PositionsBuffer, InData.UpdateData.Positions.GetNumElements());
	TangentsBuffer.InitRHIFromExisting(InData.UpdateData.TangentsBuffer, InData.UpdateData.Positions.GetNumElements());
	TexCoordsBuffer.InitRHIFromExisting(InData.UpdateData.TexCoordsBuffer, InData.UpdateData.Positions.GetNumElements(), InData.UpdateData.NumTexCoordChannels);
	ColorsBuffer.InitRHIFromExisting(InData.UpdateData.ColorsBuffer, InData.UpdateData.Positions.GetNumElements());
	IndexBuffer.InitRHIFromExisting(InData.UpdateData.TrianglesBuffer, InData.UpdateData.Positions.GetNumElements());
}

FVoxelMeshBuffers::~FVoxelMeshBuffers()
{
	PositionsBuffer.ReleaseResource();
	TangentsBuffer.ReleaseResource();
	TexCoordsBuffer.ReleaseResource();
	ColorsBuffer.ReleaseResource();
	IndexBuffer.ReleaseResource();
}

void FVoxelMeshBuffers::UpdateData(const FVoxelMeshSectionUpdateData& Data)
{
}

FVoxelMeshSceneProxy::FVoxelMeshSceneProxy(UVoxelMeshComponent* Comp)
: FPrimitiveSceneProxy(Comp)
, Component(Comp)
, LOD(0)
, CollisionTraceFlag(ECollisionTraceFlag::CTF_UseDefault)
, MaterialRelevance(Comp->GetMaterialRelevance(GetScene().GetFeatureLevel()))
{
	UE_LOG(LogTemp, Display, TEXT("Called FVoxelMeshSceneProxy()"));
}

void FVoxelMeshSceneProxy::CreateRenderThreadResources()
{
	FPrimitiveSceneProxy::CreateRenderThreadResources();
	UE_LOG(LogTemp, Display, TEXT("VoxelMeshProxy: Frame %d Created render thread resources."), GFrameCounter);
}

void FVoxelMeshSceneProxy::DestroyRenderThreadResources()
{
	Sections.Empty();
	FPrimitiveSceneProxy::DestroyRenderThreadResources();
	UE_LOG(LogTemp, Display, TEXT("VoxelMeshProxy: Frame %d Destroyed render thread resources."), GFrameCounter);

}

void FVoxelMeshSceneProxy::DrawStaticElements(FStaticPrimitiveDrawInterface* PDI)
{
	for (const auto& Section : Sections)
	{

		if (!Section.Value->bIsVisible)
		{
			continue;
		}

		auto* Material = Section.Value->SectionMaterial->GetMaterial();
		if (!Material)
		{
			// Will happen in force delete
			continue;
		}

		auto* MaterialProxy = Material->GetRenderProxy();
		FColoredMaterialRenderProxy* WireframeMaterialInstance = nullptr;
		WireframeMaterialInstance = new FColoredMaterialRenderProxy(GEngine->WireframeMaterial ? GEngine->WireframeMaterial->GetRenderProxy() : nullptr, FLinearColor(0, 0.5f, 1.f));

		FMeshBatch MeshBatch;
		DrawSection(MeshBatch, Section.Value, MaterialProxy, WireframeMaterialInstance, false, false);

		// Else the virtual texture check fails in RuntimeVirtualTextureRender.cpp:338
		// and the static mesh isn't rendered at all
		//MeshBatch.LODIndex = 0;

		if (!Section.Value->IsDynamic())
		{
			PDI->DrawMesh(MeshBatch, FLT_MAX);
			UE_LOG(LogTemp, Display, TEXT("VoxelMeshProxy: Frame %d Draw static path."), GFrameCounter);
		}

		if (RuntimeVirtualTextureMaterialTypes.Num() > 0)
		{
			// Runtime virtual texture mesh elements.
			MeshBatch.CastShadow = 0;
			MeshBatch.bUseAsOccluder = 0;
			MeshBatch.bUseForDepthPass = 0;
			MeshBatch.bUseForMaterial = 0;
			MeshBatch.bDitheredLODTransition = 0;
			MeshBatch.bRenderToVirtualTexture = 1;

			for (ERuntimeVirtualTextureMaterialType MaterialType : RuntimeVirtualTextureMaterialTypes)
			{
				MeshBatch.RuntimeVirtualTextureMaterialType = uint32(MaterialType);
				PDI->DrawMesh(MeshBatch, FLT_MAX);
			}
		}
	}

}

void FVoxelMeshSceneProxy::GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const
{
	const auto& EngineShowFlags = ViewFamily.EngineShowFlags;
	for (auto& Section : Sections)
	{
		FColoredMaterialRenderProxy* WireframeMaterialInstance = nullptr;
		WireframeMaterialInstance = new FColoredMaterialRenderProxy(GEngine->WireframeMaterial ? GEngine->WireframeMaterial->GetRenderProxy() : nullptr, FLinearColor(0, 0.5f, 1.f));

		Collector.RegisterOneFrameMaterialProxy(WireframeMaterialInstance);
		if(Section.Value->SectionMaterial && Section.Value->IsDynamic())
		{
			for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
			{
				//Check if our mesh is visible from this view
				if (VisibilityMap & (1 << ViewIndex))
				{
					const FSceneView* View = Views[ViewIndex];
					// Allocate a mesh batch and get a ref to the first element
					FMeshBatch& Mesh = Collector.AllocateMesh();
					DrawSection(Mesh, Section.Value, Section.Value->SectionMaterial->GetRenderProxy(), WireframeMaterialInstance, false, false);
					//Add the batch to the collector
					Collector.AddMesh(ViewIndex, Mesh);
				}
			}
			UE_LOG(LogTemp, Display, TEXT("VoxelMeshProxy: Frame %d Draw dynamic path."), GFrameCounter);
		}
	}
}

void FVoxelMeshSceneProxy::GetDynamicRayTracingInstances(FRayTracingMaterialGatheringContext& Context, TArray<FRayTracingInstance>& OutRayTracingInstances)
{
	int k = 0;
}

void FVoxelMeshSceneProxy::CreateSection_RenderThread(uint16 Index, FVoxelMeshRenderUpdateData&& Data)
{
	if (!Sections.Contains(Index))
	{
		FVoxelMeshSectionCreateData CreateData(MoveTemp(Data), true);

		CreateData.UpdateData.CreateRHIBuffers<true>(CreateData.bCreateDynamicSection);
		TSharedPtr<FVoxelMeshSectionData> Section = MakeShared<FVoxelMeshSectionData>(GetScene().GetFeatureLevel(), CreateData);
		Section->SectionMaterial = Component->TestMaterial;
		Sections.Add(Index, Section);
		UE_LOG(LogTemp, Display, TEXT("Frame %d Created section with ID %d"), GFrameCounter, Index);
	}
}

void FVoxelMeshSceneProxy::UpdateSection_RenderThread(uint16 Index, FVoxelMeshRenderUpdateData&& Data)
{
	auto Section = Sections.Find(Index);
	if (Section)
	{
		FVoxelMeshSectionUpdateData UpdateData(MoveTemp(Data));
		//UpdateData.CreateRHIBuffers<true>(Section->Get()->IsDynamic())
		//(*Section)->Update(Data);
	}
}

void FVoxelMeshSceneProxy::DeleteSection_RenderThread(uint16 Index)
{
	Sections.Remove(Index);
}

FPrimitiveViewRelevance FVoxelMeshSceneProxy::GetViewRelevance(const FSceneView* View) const
{
	FPrimitiveViewRelevance Result;
	Result.bDrawRelevance = IsShown(View);
	Result.bShadowRelevance = IsShadowCast(View);
	Result.bDynamicRelevance = true;
	Result.bRenderInMainPass = ShouldRenderInMainPass();
	Result.bUsesLightingChannels = GetLightingChannelMask() != GetDefaultLightingChannelMask();
	Result.bRenderCustomDepth = ShouldRenderCustomDepth();
	Result.bTranslucentSelfShadow = bCastVolumetricTranslucentShadow;
	MaterialRelevance.SetPrimitiveViewRelevance(Result);
	Result.bVelocityRelevance = IsMovable() && Result.bOpaque && Result.bRenderInMainPass;
	return Result;
}

bool FVoxelMeshSceneProxy::CanBeOccluded() const
{
	return !MaterialRelevance.bDisableDepthTest;
}

uint32 FVoxelMeshSceneProxy::GetMemoryFootprint() const
{
	return uint32();
}

SIZE_T FVoxelMeshSceneProxy::GetTypeHash() const
{
	return SIZE_T();
}

uint32 FVoxelMeshSceneProxy::GetAllocatedSize() const
{
	return uint32();
}

bool FVoxelMeshSceneProxy::HasDistanceFieldRepresentation() const
{
	return false;
}

bool FVoxelMeshSceneProxy::HasDynamicIndirectShadowCasterRepresentation() const
{
	return false;
}

void FVoxelMeshSceneProxy::DrawSection(FMeshBatch& Mesh, const TSharedPtr<FVoxelMeshSectionData>& Section, const FMaterialRenderProxy* MaterialRenderProxy, const FMaterialRenderProxy* WireframeMaterial, bool bEnableTessellation, bool bWireframe) const
{
	check(MaterialRenderProxy);
	Mesh.VertexFactory = &Section->VertexFactory;
	Mesh.MaterialRenderProxy = MaterialRenderProxy;

	Mesh.ReverseCulling = IsLocalToWorldDeterminantNegative();
	Mesh.Type = PT_TriangleList;
	Mesh.DepthPriorityGroup = SDPG_World;
	Mesh.bUseWireframeSelectionColoring = IsSelected() && bWireframe; // Else mesh LODs view is messed up when actor is selected
	Mesh.bCanApplyViewModeOverrides = false;
	Mesh.VisualizeLODIndex = 0 % GEngine->LODColorationColors.Num();
	Mesh.LODIndex = 0;
	Mesh.CastShadow = false;
	Mesh.bDitheredLODTransition = 0;
	Mesh.bWireframe = false; //WireframeMaterial != nullptr;
	Mesh.CastRayTracedShadow = false;
	Mesh.SegmentIndex = 0;
	Mesh.bDisableBackfaceCulling = 1;

	FMeshBatchElement& BatchElement = Mesh.Elements[0];
	BatchElement.IndexBuffer = &Section->Buffers.IndexBuffer;
	BatchElement.FirstIndex = 0;
	BatchElement.NumPrimitives = Section->Buffers.IndexBuffer.Num()/3;
	BatchElement.MinVertexIndex = 0;
	BatchElement.MaxVertexIndex = Section->Buffers.PositionsBuffer.Num() - 1;
}

bool FVoxelMeshSceneProxy::ShouldDrawComplexCollisions(const FEngineShowFlags& EngineShowFlags) const
{
	return false;
}

