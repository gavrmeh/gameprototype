// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "BasicCharacterMovementComponent.generated.h"
/*
class ABasicCharacter;

const float WalkableFloorZ = 0.6;

USTRUCT(Blueprintable)
struct GAMEPROTOTYPE_API FEdgeTolerance
{
GENERATED_BODY()
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	float RadiusTolerance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector BoxExtentsTolerance;
};

USTRUCT(Blueprintable)
struct GAMEPROTOTYPE_API FFloorSearchResult
{
GENERATED_BODY()
	FFloorSearchResult() = default;
	FFloorSearchResult(const FHitResult& Hit);
	FFloorSearchResult(const TArray<FHitResult>& Hits);
	FFloorSearchResult(const FFloorSearchResult& Other) = default;
	FFloorSearchResult(FFloorSearchResult&& Other) = default;
	FFloorSearchResult& operator=(const FFloorSearchResult& Other) = default;
	FFloorSearchResult& operator=(FFloorSearchResult&& Other) = default;

	UPROPERTY(BlueprintReadWrite)
	FVector Normal = FVector::ZeroVector;

	FVector FloorHitPosition = FVector::ZeroVector;

	FVector ImpactNormal = FVector::ZeroVector;

	TArray<TWeakObjectPtr<UPhysicalMaterial>> PhysicalMaterials;

	TArray<TWeakObjectPtr<USceneComponent>> Components;

	bool bIsValidFloor = false;


};
/**
 * 
 */
/*
UENUM()
enum ELegSide
{
	Left, Right
};

USTRUCT(BlueprintType)
struct GAMEPROTOTYPE_API FCharacterLeg
{
	FName LegBone;
	TEnumAsByte<ELegSide> LegSide;
};

struct GAMEPROTOTYPE_API FCharacterLegData
{
	FCharacterLeg Leg;
	FFloorSearchResult Floor;
	int32 BoneIndex;
	FVector LocalLegPosition;
};

UCLASS(Blueprintable)
class GAMEPROTOTYPE_API UBasicCharacterMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()

	FVector Velocity;

	bool bShouldJump = false;

	FVector2D Rotation;

	FVector InputVector = FVector::ZeroVector;

	TArray<FCharacterLegData> LegData;

	FFloorSearchResult MainCollisionFloorSearchResult;

	UPrimitiveComponent* MainCollision = nullptr;

	FCharacterLegData* FrontLegData;
	FCharacterLegData* BackLegData;
	FCharacterLegData* RightLegData;
	FCharacterLegData* LeftLegData;

	bool bIsOnFloor = true;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsZRotationFixed = true;

	UPROPERTY(EditAnywhere)
	float MaxSpeed = 500;

	UPROPERTY(EditAnywhere)
	float RunningSpeedMultiplyer = 1.5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CrouchingSpeedMultiplyer = 0.6;

	UPROPERTY(EditAnywhere)
	float Acceleration = 2048;

	UPROPERTY(EditAnywhere)
	float MaxStepUpHeight = 45;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxRotationSpeed = PI;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxDistanceToFloor = 2.5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LegTraceDistance = 20;

	UPROPERTY(BlueprintReadWrite)
	TEnumAsByte<EMovementMode> MovementMode;

	UPROPERTY(BlueprintReadWrite)
	TObjectPtr<ABasicCharacter> Character;

	UPROPERTY(EditAnywhere)
	FEdgeTolerance EdgeTolerance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FloorCheckDistance = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ClampMin="0", ClampMax="1"))
	float SlideOffShapeRadiusPercentage = 0.6;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ClampMin="0", ClampMax="90"))
	float MaxSlidingAngle = 60;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ClampMin="0"))
	float JumpHeight = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanChangeRoll = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanChangePitch = false;

	UFUNCTION(BlueprintCallable)
	FVector GetVelocity() const
	{
		return Velocity;
	};

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FCharacterLeg> Legs;

	//This component is expected to be the root or 
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName MainCollisionComponentName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 MinNumLegsForStability = 2;

	UFUNCTION(BlueprintCallable)
		void AddRotation(const FVector2D& InRotation)
	{
		Rotation = InRotation;
		Rotation.ClampAxes(-MaxRotationSpeed, MaxRotationSpeed);
	}

	virtual float GetMaxSpeed() const override;

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void SetUpdatedComponent(USceneComponent* NewComponent) override;

	FFloorSearchResult FindFloor(const UPrimitiveComponent* Primitive);

	void UpdateFloor();

	bool IsStandingOnFloor();

	void Move(float DeltaTime);

	void Walk(float DeltaTime);

	void Fall(float DeltaTime);

	void Swim(float DeltaTime);

	void Fly(float DeltaTime);

	void WalkNavMesh(float DeltaTime);

	void UpdateVelocityAuthority(float DeltaTime);

	void UpdatedVelocitySimulatedProxy(float DeltaTime);

	void ChooseMovementMode();

	bool StepUp(const FVector& DesiredMovement, const FHitResult& Hit);

	static void AdjustCollisionShape(FCollisionShape& Shape);

	static bool IsWalkableFloor(const FHitResult& Hit);

	bool ShouldSlideAlongWall(const FVector& MovementVector, const FHitResult& WallHit);

	void Perch();

	bool MoveAlongWall(const FVector& DesiredMovement, const FHitResult& WallHit);

	bool ShouldSlideOffFloor();

	bool IsWithingEdgeTolerance(const FFloorSearchResult& Floor) const;

	void HandleImpact(const FHitResult& Hit, const FVector& MovementDir);

	FVector GetMovementAlongRamp(const FVector& InMovement, const FFloorSearchResult& Surface);

	float GetUpdatedComponentLowerPointZ() const;

	void RotateAuthority();

	void UpdateDistanceFromFloor();

	void TraceLeg(FCharacterLegData& Leg);

	bool DoesChangeRoll() {return bCanChangeRoll;}

	bool DoesChangePitch() {return bCanChangePitch && Legs.Num() > 2; }

	FVector GetFrontTracePos() const;

	FVector GetBackTracePos() const;

	FVector GetRightTracePos() const;
	
	FVector GetLeftTracePos() const;
};
*/