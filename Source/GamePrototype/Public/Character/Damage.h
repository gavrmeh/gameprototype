#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "Damage.generated.h"

class UCharacterStateComponent;

USTRUCT(BlueprintType, Blueprintable)
struct GAMEPROTOTYPE_API FDamageComponent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Value;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBasicDamageType* DamageType = nullptr;
};

USTRUCT(BlueprintType, Blueprintable)
struct GAMEPROTOTYPE_API FDamage
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FDamageComponent> DamageComponents;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	APawn* DamageInstigator;
};



UCLASS(BlueprintType, Blueprintable)
class GAMEPROTOTYPE_API UBasicDamageType : public UObject
{
	GENERATED_BODY()
public:
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	float CalculateDamageOutput(float BasicDamage, UCharacterStateComponent* Character);

	virtual float CalculateDamageOutput_Implementation(float BasicDamage, UCharacterStateComponent* Character);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	float CalculateIncomingDamage(float IncomingDamage, UCharacterStateComponent* Receiver, UCharacterStateComponent* Instigator);

	virtual float CalculateIncomingDamage_Implementation(float IncomingDamage, UCharacterStateComponent* Receiver, UCharacterStateComponent* Instigator);
};

UCLASS(BlueprintType, Blueprintable)
class GAMEPROTOTYPE_API UDefaultDamageType : public UBasicDamageType
{

	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName DamageSourceTag;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSet<FName> ResistedByTags;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSet<FName> ScalesByTags;


	virtual float CalculateDamageOutput_Implementation(float IncomingDamage, UCharacterStateComponent* Receiver) override;

	virtual float CalculateIncomingDamage_Implementation(float IncomingDamage, UCharacterStateComponent* Receiver, UCharacterStateComponent* Instigator) override;

};