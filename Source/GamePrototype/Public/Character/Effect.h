#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "CharacterAttributes.h"
#include "Damage.h"
#include "Effect.generated.h"

UCLASS(BlueprintType, Blueprintable)
class GAMEPROTOTYPE_API UStatusEffect : public UObject
{
GENERATED_BODY()
public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	TArray<FName> Tags;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCharacterStateComponent* AffectedCharacter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, SaveGame)
	TSet<UAttributeModifier*> Modifiers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bShouldTick = false;

	UFUNCTION(BlueprintCallable)
		void SetAffectedCharacter(UCharacterStateComponent* NewAffectedCharacter)
	{
		AffectedCharacter = NewAffectedCharacter;
	}

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void TickStatusEffect(float DeltaTime);

	virtual void TickStatusEffect_Implementation(float DeltaTime)
	{

	}

	//Override this if you need custom behaviour when character with this status deals damage
	UFUNCTION(BlueprintCallable)
	virtual void OnDealDamage(const FDamage& Damage) {};

	//Override this if you need custom behaviour when character with this status takes damage
	UFUNCTION(BlueprintCallable)
	virtual void OnTakeDamage(const FDamage& Damage) {};

	UFUNCTION(BlueprintCallable)
	virtual void OnApplyEffect(ABasicCharacter* TargetCharacter) {};
};