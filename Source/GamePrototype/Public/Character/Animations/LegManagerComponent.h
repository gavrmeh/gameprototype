#pragma once

#include "LegTraceComponent.h"
#include "Containers/CircularBuffer.h"
#include "LegManagerComponent.generated.h"
/*
UCLASS(Blueprintable, ClassGroup = (Common), meta = (BlueprintSpawnableComponent))
class GAMEPROTOTYPE_API ULegManagerComponent : public UActorComponent
{
	GENERATED_BODY()
public:
	
	ULegManagerComponent()// : Super()
	{
		PrimaryComponentTick.TickGroup = TG_PrePhysics;
		PrimaryComponentTick.bCanEverTick = true;
		bAutoActivate = true;
	};

	virtual void BeginPlay() override;

	virtual void OnRegister() override
	{
		Super::OnRegister();
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ULegTraceComponent*> Legs;

	UBasicCharacterMovementComponent* MovementComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ELegMovementState> LegMovementState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DirectionDeltaThreshold = 1;

	USceneComponent* Root = nullptr;

	FRotator PreviousRotation = FRotator::ZeroRotator;

	FVector DEBUGPreviousPosition = FVector::ZeroVector;

	float DirectionDeltaAngle = 0;

	int32 TrajectoryStartIndex = 0;

	bool bIsMoving = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 NumCachedTrajectoryPoints = 8;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StepTime = 1;

	float TrajectoryCacheDeltaTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CatchUpStepPart = 0.25;

	TCircularBuffer<FTransform> CachedTrajectory = TCircularBuffer<FTransform>(2);

	float TimeSinceLastTrajectoryCache = 0;

	size_t LastLegIndex = 0;
	virtual void TickComponent(float DeltaTime,
		enum ELevelTick TickType,
		FActorComponentTickFunction* ThisTickFunction
	);

	float GetAbsRelativeDirectionDelta() const
	{
		return FMath::Min(FMath::Abs(DirectionDeltaAngle)/DirectionDeltaThreshold, 1.f);
	}

	bool CanLegStep(ULegTraceComponent* LegTraceComponent) const
	{
		//This check will be different is we are running, when we run we do not need to wait for lega leg to be planted to start stepping
		bool Result = false;


		switch (LegMovementState)
		{
			case ELegMovementState::Standing:
				for (auto Leg : Legs)
				{
					if (Leg != LegTraceComponent)
					{
						Result |= Leg->bIsPlanted;
					}
				}
				Result &= !LegTraceComponent->IsInRestingPosition();
			break;
			case ELegMovementState::Walking:
				if (LegTraceComponent->bHasStepped)
				{
					LegTraceComponent->bHasStepped = false;
				}
				else
				{
					for (auto Leg : Legs)
					{
						if (Leg != LegTraceComponent)
						{
							Result |= Leg->bIsPlanted;
						}
					}
				}

			break;
			case ELegMovementState::Running:
				Result = true;
				LegTraceComponent->bHasStepped = false;
			break;
			case ELegMovementState::Other:
				
			break;
		};

		switch (LegTraceComponent->LegSide)
		{
		case ELegSide::Left:

			
		break;
		case ELegSide::Right:

		break;
		}

		return Result;
	}

	uint32 GetTrajectoryMiddleIndex() const
	{
		return (TrajectoryStartIndex + NumCachedTrajectoryPoints/2)%NumCachedTrajectoryPoints;
	}

	float GetStepPartPerTrajectoryLine() const
	{
		return 2*CatchUpStepPart/NumCachedTrajectoryPoints;
	};
};
*/