#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "Curves/CurveFloat.h"
#include "LegTraceComponent.generated.h"
/*
UENUM()
enum ELegMovementState
{
	Standing, Walking, Running, Other
};

UENUM()
enum ELegSide
{
	Right, Left
};

UENUM()
enum EStepType
{
	FullStep, HalfStep
};

class ULegManagerComponent;
class UBasicCharacterMovementComponent;
UCLASS(Blueprintable, ClassGroup = (Common), meta = (BlueprintSpawnableComponent))
class GAMEPROTOTYPE_API ULegTraceComponent : public USceneComponent
{
	GENERATED_BODY()
public:
	ULegTraceComponent()
	{
		PrimaryComponentTick.TickGroup = TG_DuringPhysics;
		PrimaryComponentTick.bCanEverTick = true;
		bAutoActivate = true;
		SetComponentTickEnabled(true);
	}

	virtual void BeginPlay() override;

	FVector PreviousStepPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsPlanted = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StepAlpha = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TotalStepTime = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UCurveFloat> StepHeightCurve;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UCurveFloat> StepPositionCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UCurveFloat> SubstepPositionCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector StepDirection = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StepLength = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StepHeight = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsStepping = false;

	bool bIsSteppingFromBack = false;

	bool bIsStepTargetFixed = false;

	TEnumAsByte<EStepType> StepType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bShouldDebugDraw = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ELegSide> LegSide;

	bool bHasStepped = false;

	ULegManagerComponent* LegManager = nullptr;

	UBasicCharacterMovementComponent* MovementComponent = nullptr;

	FVector StepTarget;

	FVector LocalFootRestingPosition;


	bool WantsToReplant()
	{
		return (StepTarget - GetComponentLocation()).SizeSquared() > FMath::Square(StepLength);
	}

	bool bCanReplant = true;

	UFUNCTION(BlueprintCallable)
	bool IsInRestingPosition() const;

	FTransform PreviousComponentToWorld;

	bool bIsCatchingUp = false;
	void UpdateLeg(float DeltaTime);
protected:
	
	void StartStep();

	void UpdateStepTarget();

	void MakeStep(float DeltaTime);

	void ChooseStepType();
};
*/