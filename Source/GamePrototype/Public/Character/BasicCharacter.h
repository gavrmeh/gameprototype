// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/BoxComponent.h"
#include "Components/ContainerComponent.h"
#include "Camera/CameraComponent.h"
#include "BasicCharacterMovementComponent.h"
#include "InputAction.h"
#include "Inventory/EquippableItem.h"
#include "Effect.h"
#include "UObject/ConstructorHelpers.h"
#include "Data/CharacterAttackAnimTable.h"
#include "Inventory/Weapon.h"
#include "Animations/LegManagerComponent.h"
#include "Interfaces/BasicCharacterInterface.h"
#include "Components/AttributesComponent.h"
#include "BasicCharacter.generated.h"


UCLASS()
class GAMEPROTOTYPE_API ABasicCharacter : public ACharacter, public IBasicCharacterInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABasicCharacter();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DoSmth();

	void DoSmth_Implementation()
	{
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 15, FColor(255,255,255,255), FString(TEXT("Called DoSmth")), false, FVector2D(1,1));
	}

	virtual void PawnClientRestart() override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UContainerComponent> Inventory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UCharacterStateComponent> CharacterState;
	//This should be moved to local player
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UInputMappingContext> InputMapping;
	virtual void ReceiveDamage_Implementation(const FDamage& Damage) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UDataTable> CharacterAttackAnimTable;

	UFUNCTION(BlueprintCallable)
	void Move(const FVector2D& Input)
	{
		AddMovementInput(GetActorForwardVector()*Input.Y);
		AddMovementInput(GetActorRightVector()*Input.X);
	};

	UFUNCTION(BlueprintCallable)
	void Turn(float Input)
	{
		AddControllerYawInput(Input);
	};

};
