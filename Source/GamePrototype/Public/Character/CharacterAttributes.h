#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "CharacterAttributes.generated.h"

UENUM()
enum EAttributeType
{
	Flat, FlatRandomized, Percentage
};

class UCharacterStateComponent;

USTRUCT(Blueprintable, BlueprintType)
struct GAMEPROTOTYPE_API FAttributeValue
{
GENERATED_BODY()

public:	
	FAttributeValue(float InMaxValue, float InCurrentValue)
	: MaxValue(InMaxValue), CurrentValue(InCurrentValue)
	{
		
	}

	FAttributeValue()
	{

	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxValue = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CurrentValue = 0;
};

UCLASS(Blueprintable, EditInlineNew)
class GAMEPROTOTYPE_API UAttributeModifier : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EAttributeType> ModifierType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MinValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Value;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName AffectedAttribute;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText AffectedAttributeText;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FText GetModifierInfo() const;

	virtual FText GetModifierInfo_Implementation() const
	{
		return FText();
	}

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FAttributeValue GetValue(const UCharacterStateComponent* Character);

	virtual FAttributeValue GetValue_Implementation(const UCharacterStateComponent* Character)
	{
		if (ModifierType == EAttributeType::FlatRandomized)
		{
			float Val = FMath::RandRange(MinValue, MaxValue);
			return FAttributeValue(Val, Val);
		}
		else
		{
			return FAttributeValue(Value, Value);
		}
	}

};

UCLASS(Blueprintable, EditInlineNew)
class GAMEPROTOTYPE_API UBasicAttribute : public UDataAsset
{
	GENERATED_BODY()
public:
	UBasicAttribute() = default;

	/*
	Name of the attribute which is affected by this.
	For basic attributes like Strength or Intelligence this should be equal to AttributeName
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName AffectedAttributeName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName AttributeName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText AttributeDescriptionText;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FText GetAttributeInfoText() const;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText AttributeNameText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsRecievedFromModifier = false;

	virtual FText GetAttributeInfoText_Implementation() const
	{
		return AttributeDescriptionText;
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EAttributeType> AttributeType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Meta=(ClampMin="0"))
	float Value = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bShouldTick = false;

	UFUNCTION(BlueprintCallable)
	virtual void Tick(float DeltaTime, UCharacterStateComponent* ParentCharacter){};

	float TotalStaticValue = 0;

	UFUNCTION(BlueprintCallable)
	virtual FAttributeValue GetValue(UCharacterStateComponent* ParentCharacter) const
	{
		float TotalFlatValueFromModifiers = 0;
		float TotalIncrease = 1;
		for (UAttributeModifier* Modifier : Modifiers)
		{
			if (Modifier->ModifierType == EAttributeType::Percentage)
			{
				TotalIncrease += Modifier->GetValue(ParentCharacter).CurrentValue;
			}
			else
			{
				TotalFlatValueFromModifiers += Modifier->GetValue(ParentCharacter).CurrentValue;
			}
		}
		float TotalValue = (Value + TotalFlatValueFromModifiers) * TotalIncrease;
		return FAttributeValue(TotalValue, TotalValue); 
	};

	TSet<UAttributeModifier*> Modifiers;
};

UCLASS(Blueprintable, BlueprintType)
class GAMEPROTOTYPE_API UCharacterStateAttribute : public UBasicAttribute
{
	GENERATED_BODY()
public:
	
	UCharacterStateAttribute()
	{
		bShouldTick = true;
	}

	virtual FAttributeValue GetValue(UCharacterStateComponent* ParentCharacter) const
	{
		float TotalFlatValueFromModifiers = 0;
		float TotalIncrease = 1;
		for (UAttributeModifier* Modifier : Modifiers)
		{
			if (Modifier->ModifierType == EAttributeType::Percentage)
			{
				TotalIncrease += Modifier->GetValue(ParentCharacter).CurrentValue;
			}
			else
			{
				TotalFlatValueFromModifiers += Modifier->GetValue(ParentCharacter).CurrentValue;
			}
		}
		float TotalValue = (Value + TotalFlatValueFromModifiers) * TotalIncrease;
		return FAttributeValue(TotalValue, CurrentValue);
	};
	virtual void Tick(float DeltaTime, UCharacterStateComponent* ParentCharacter);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CurrentValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName RegenAttributeName;
};
