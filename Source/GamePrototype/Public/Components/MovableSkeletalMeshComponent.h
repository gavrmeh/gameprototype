#pragma once

#include "Components/SkeletalMeshComponent.h"
#include "MovableSkeletalMeshComponent.generated.h"

UCLASS(Blueprintable, ClassGroup = (Rendering, Common), hidecategories = Object, config = Engine, editinlinenew, meta = (BlueprintSpawnableComponent))
class GAMEPROTOTYPE_API UMovableSkeletalMeshComponent : public USkeletalMeshComponent
{
	GENERATED_BODY()
public:
	UMovableSkeletalMeshComponent();

	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<UPrimitiveComponent*> CollisionComponents;

	UFUNCTION(BlueprintCallable)
	void AddCollisionComponent(UPrimitiveComponent* Component)
	{
		if(!CollisionComponents.Contains(Component))
		{
			CollisionComponents.Push(Component);
		}
	};
protected:
	
	bool MoveComponentImpl(const FVector& Delta, const FQuat& NewRotation, bool bSweep, FHitResult* OutHit, EMoveComponentFlags MoveFlags, ETeleportType Teleport);
};