#pragma once

#include "Components/PrimitiveComponent.h"
#include "AggregatedCollisionComponent.generated.h"

UCLASS(ClassGroup = (Utility, Common), BlueprintType, hideCategories = (Trigger, PhysicsVolume), meta = (BlueprintSpawnableComponent, IgnoreCategoryKeywordsInSubclasses, ShortTooltip = "An Aggregated Collision Component is a component which is intended to be used as a root component which contains multiple collision bodies"))
class GAMEPROTOTYPE_API UAggregatedCollisionComponent : public USceneComponent
{
	GENERATED_BODY()
public:
	UAggregatedCollisionComponent();

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<UPrimitiveComponent*> CollisionComponents;

	UFUNCTION(BlueprintCallable)
	void AddCollisionComponent(UPrimitiveComponent* Component);

	UFUNCTION(BlueprintCallable)
	bool RemoveCollisionComponent(UPrimitiveComponent* Component);

	virtual FBoxSphereBounds CalcBounds(const FTransform& LocalToWorld) const override;
protected:

	bool MoveComponentImpl(const FVector& Delta, const FQuat& NewRotation, bool bSweep, FHitResult* OutHit, EMoveComponentFlags MoveFlags, ETeleportType Teleport);

};