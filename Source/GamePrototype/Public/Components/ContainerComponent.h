#pragma once

#include "Components/ActorComponent.h"
#include "Inventory/Item.h"
#include "ContainerComponent.generated.h"

UCLASS(Blueprintable, ClassGroup = (Common), hidecategories = Object, BlueprintType, meta = (BlueprintSpawnableComponent))
class GAMEPROTOTYPE_API UContainerComponent : public UActorComponent
{
GENERATED_BODY()

public:
	UContainerComponent();

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* TickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AItem*> Items;

	UFUNCTION(BlueprintCallable)
	bool ContainsItem(AItem* Item) const
	{
		auto Result = Items.FindByPredicate([&Item](AItem* Val)->bool{return Item->ItemName == Val->ItemName;});
		return Item!=nullptr;
	};

	UFUNCTION(BlueprintCallable)
	AItem* GetItemByOtherItemName(AItem* Item)
	{
		auto Result = Items.FindByPredicate([&Item](AItem* Val)->bool {return Item->ItemName == Val->ItemName; });
		if (Result)
		{
			return *Result;
		}
		else
		{
			return nullptr;
		}
	}
};