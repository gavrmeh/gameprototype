#pragma once

#include "Components/ActorComponent.h"

#include "Character/CharacterAttributes.h"
#include "Inventory/EquippableItem.h"
#include "Character/Effect.h"
#include "Inventory/EquipmentSlot.h"
#include "Components/ContainerComponent.h"
#include "AttributesComponent.generated.h"


UCLASS(BlueprintType, Blueprintable)
class GAMEPROTOTYPE_API UCharacterStateComponent : public UActorComponent
{
	GENERATED_BODY()

	APawn* ParentPawn = nullptr;

	UContainerComponent* Container = nullptr;

public:
	UCharacterStateComponent() = default;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TObjectPtr<APawn> OwningPawn;

	virtual void BeginPlay() override;

	virtual void OnSetParentPawn(APawn* NewPawn);

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* TickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, SaveGame)
		TArray<UEquipmentSlot*> EquipmentSlots;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, SaveGame)
		TArray<UBasicAttribute*> Attributes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, SaveGame)
		TSet<UStatusEffect*> StatusEffects;

	//Cached items
	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
		TArray<AEquippableItem*> EquippedItems;

	UFUNCTION(BlueprintCallable)
		FAttributeValue GetTotalAttributeValue(FName AttributeName)
	{
		UBasicAttribute* Attr = GetAttributeByName(AttributeName);
		return Attr ? Attr->GetValue(this) : FAttributeValue();
	}

	UFUNCTION(BlueprintCallable)
		UBasicAttribute* GetAttributeByName(FName AttributeName)
	{
		for (auto Attribute : Attributes)
		{
			if (Attribute->AttributeName == AttributeName)
			{
				return Attribute;
			}
		}
		return nullptr;
	}

	UFUNCTION(BlueprintCallable)
	UContainerComponent* GetContainer() const
	{
		return Container;
	};

	UFUNCTION(BlueprintCallable)
		void ApplyStatusEffect(UStatusEffect* Effect)
	{
		StatusEffects.Add(Effect);
		ApplyModifiers(Effect->Modifiers);
		Effect->AffectedCharacter = this;
	}

	UFUNCTION(BlueprintCallable)
		void RemoveStatusEffect(UStatusEffect* Effect)
	{
		StatusEffects.Remove(Effect);
		RemoveModifiers(Effect->Modifiers);
		Effect->AffectedCharacter = nullptr;
	}

	UFUNCTION(BlueprintCallable)
		void ApplyModifiers(const TSet<UAttributeModifier*>& Modifiers)
	{
		for (auto Modifier : Modifiers)
		{
			bool bHasModifiedAttribute = false;
			for (auto Attribute : Attributes)
			{
				if (Modifier->AffectedAttribute == Attribute->AttributeName)
				{
					Attribute->Modifiers.Add(Modifier);
					bHasModifiedAttribute = true;
				}
			}
			if (!bHasModifiedAttribute)
			{
				int32 ID = Attributes.Add(NewObject<UBasicAttribute>(UBasicAttribute::StaticClass()));
				Attributes[ID]->bIsRecievedFromModifier = true;
				Attributes[ID]->Modifiers.Add(Modifier);
			}
		}
	}

	UFUNCTION(BlueprintCallable)
		void RemoveModifiers(const TSet<UAttributeModifier*>& Modifiers)
	{
		TArray<UBasicAttribute*> AttrsToRemove;
		for (auto Modifier : Modifiers)
		{
			for (auto Attribute : Attributes)
			{
				if (Modifier->AffectedAttribute == Attribute->AttributeName)
				{
					Attribute->Modifiers.Remove(Modifier);
				}
				if (Attribute->bIsRecievedFromModifier && Attribute->GetValue(this).CurrentValue == 0)
				{
					AttrsToRemove.Push(Attribute);
				}
			}
		}
		for (auto Attr : AttrsToRemove)
		{
			Attributes.Remove(Attr);
		}
	}

	UFUNCTION(BlueprintCallable)
		void EquipItem(AEquippableItem* Item)
	{
		if (Item)
		{
			TArray<UEquipmentSlot*> UsedSlots;
			for (FName SlotName : Item->UsedSlotNames)
			{
				for (auto Slot : EquipmentSlots)
				{
					if (Slot->SlotName == SlotName)
					{
						UsedSlots.Push(Slot);
					}
				}
			}
			if (UsedSlots.Num())
			{
				for (auto Slot : UsedSlots)
				{
					Unequip(Slot);
					Slot->Item = Item;
				}
				EquippedItems.Add(Item);
				Item->OnEquip(this);
			}
		}
	}

	UFUNCTION(BlueprintCallable)
		void Unequip(UEquipmentSlot* Slot)
	{
		AEquippableItem* Item = Slot->Item;
		if (Item)
		{
			for (auto SlotName : Item->UsedSlotNames)
			{
				auto CurrentSlot = GetEquipmentSlotByName(SlotName);
				CurrentSlot->Item = nullptr;
			}
			Item->OnUnequip();
			EquippedItems.Remove(Item);
		}
	}

	UFUNCTION(BlueprintCallable)
		UEquipmentSlot* GetEquipmentSlotByName(const FName SlotName)
	{

		auto Result = EquipmentSlots.FindByPredicate([SlotName](UEquipmentSlot* Slot)->bool
			{
				return Slot->SlotName == SlotName;
			});
		if (Result)
		{
			return *Result;
		}
		else
		{
			return nullptr;
		}
	}

	UFUNCTION(BlueprintCallable)
		FName GetSocketNameBySlotNames(const TArray<FName>& SlotNames)
	{
		return FName();
	};
};