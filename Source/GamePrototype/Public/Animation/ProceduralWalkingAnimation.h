#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNodeBase.h"
#include "AnimGraphNode_SkeletalControlBase.h"
#include "Curves/CurveFloat.h"
#include "TwoBoneIK.h"
#include "FABRIK.h"
#include "CCDIK.h"
#include "BoneControllers/AnimNode_SkeletalControlBase.h"
#include "ProceduralWalkingAnimation.generated.h"


USTRUCT(BlueprintType)
struct GAMEPROTOTYPE_API FLegDescriptor
{
    GENERATED_BODY()

    FLegDescriptor() = default;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FName FootBoneName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 NumBones = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FVector LegDirection = FVector::ForwardVector;

    TArray<FFABRIKChainLink> IKChainLinks;

    TArray<FTransform> BoneTransforms;

    TArray<FCompactPoseBoneIndex> BoneIndecies;

    void InitializeTransforms(FAnimInstanceProxy* MyAnimInstanceProxy, FCSPose<FCompactPose>& MeshBases);

    FVector ComponentSpaceStepTarget;
    FVector ComponentSpaceLastPlantPosition;
    FVector ComponentSpaceDefaultFootPosition;
    FVector ComponentSpaceCurrentPosition;

    float MaximumReach = 0;

};

USTRUCT(BlueprintType)
struct GAMEPROTOTYPE_API FLegGroupDescriptor
{
    GENERATED_BODY()

    FLegGroupDescriptor() = default;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<int32> LegIndecies;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ClampMin="0", ClampMax="1"))
    float StepStartTime = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ClampMin="0", ClampMax="1"))
    float StepEndTime = 1;
};

USTRUCT(BlueprintType)
struct FProceduralWalkAnimNode : public FAnimNode_SkeletalControlBase
{
    GENERATED_BODY()
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    mutable FName RootBoneName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FLegDescriptor> Legs;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FLegGroupDescriptor> LegGroups;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AlwaysAsPin))
    mutable FVector MovementDir;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AlwaysAsPin))
    mutable FRotator Rotation;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    bool bDrawDebugData = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float StepForwardDistance;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float TotalStepTime = 1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MaxStepDistanceSpeed = 100;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float MaxStepHeight;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TObjectPtr<UCurveFloat> StepHeightCurve;

    float CurrentStepTime = 0;

    bool bHasCachedLegs = false;

    FAnimInstanceProxy* AnimInstanceProxy;

    FVector LocalSpaceMovementDirection;
    FVector LocalSpaceRotation;
    float CurrentVelocity = 0;

    // FAnimNode_Base interface
    virtual void GatherDebugData(FNodeDebugData& DebugData) override;
    // End of FAnimNode_Base interface

    // FAnimNode_SkeletalControlBase interface
    virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
    virtual void EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms) override;
    virtual bool IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones) override;
    // End of FAnimNode_SkeletalControlBase interface

    void UpdateLegGroup(FLegGroupDescriptor& LegGroup, FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms);

    FProceduralWalkAnimNode();

    void UpdateLegStep(FLegDescriptor& Leg, float StepAlpha, FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms);

    void UpdateLegKeepPosition(FLegDescriptor& Leg, FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransform);

    void UpdateLegStepTarget(FLegDescriptor& Leg);

    void InitLegs(FAnimInstanceProxy* MyAnimInstanceProxy, FCSPose<FCompactPose>& MeshBases);

private:
    // FAnimNode_SkeletalControlBase interface
    virtual void InitializeBoneReferences(const FBoneContainer& RequiredBones) override;
    // End of FAnimNode_SkeletalControlBase interface};

    FTransform ConvertToWorldSpaceTransform(const FTransform& ComponentSpaceTransform);

    FTransform ConvertToComponentSpaceTransform(const FTransform& WorldSpaceTransform);

    FVector ConvertToWorldSpace(const FVector& ComponentSpacePosition);
    FVector ConvertToComponentSpace(const FVector& WorldSpacePosition);

    void UpdateBoneTransforms(FLegDescriptor& Leg, FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms);
};
UCLASS()
class UProceduralWalkAnimGraphNode : public UAnimGraphNode_SkeletalControlBase
{
    GENERATED_BODY()

public:

    UPROPERTY(EditAnywhere, Category = "Settings")
        FProceduralWalkAnimNode Node;

    //~ Begin UEdGraphNode Interface.
    virtual FLinearColor GetNodeTitleColor() const override;
    virtual FText GetTooltipText() const override;
    virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
    //~ End UEdGraphNode Interface.

    //~ Begin UAnimGraphNode_Base Interface
    virtual FString GetNodeCategory() const override;
    //~ End UAnimGraphNode_Base Interface

    UProceduralWalkAnimGraphNode(const FObjectInitializer& ObjectInitializer);
    // UAnimGraphNode_SkeletalControlBase interface
protected:
    virtual FText GetControllerDescription() const override{return FText();};
    virtual const FAnimNode_SkeletalControlBase* GetNode() const override { return &Node; }
    // End of UAnimGraphNode_SkeletalControlBase interface
};

