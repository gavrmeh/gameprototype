#pragma once

#include "Engine/DataTable.h"
#include "Animation/AnimMontage.h"
#include "CharacterAttackAnimTable.generated.h"

USTRUCT(BlueprintType)
struct GAMEPROTOTYPE_API FCharacterAttackAnimTableRow : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSoftObjectPtr<UAnimMontage> Animation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 NumAnimations;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName WeaponType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName SkeletonClassName;
};