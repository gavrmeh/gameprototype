#pragma once

#include "CoreMinimal.h"

#include "UObject/Interface.h"

#include "../Inventory/EquipmentSlot.h"

#include "Character/Damage.h"

#include "BasicCharacterInterface.generated.h"

UINTERFACE()
class GAMEPROTOTYPE_API UBasicCharacterInterface : public UInterface
{
	GENERATED_BODY()
};

class UEquipmentSlot;

class GAMEPROTOTYPE_API IBasicCharacterInterface : public IInterface
{
	GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnUpdateSkeletalMesh();

	virtual void OnUpdateSkeletalMesh_Implementation() {};

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void ReceiveDamage(const FDamage& Damage);

	virtual void ReceiveDamage_Implementation(const FDamage& Damage) {};

};