#pragma once

#include "CoreMinimal.h"

#include "UObject/Interface.h"
#include "Interactions/InteractionContext.h"
#include "Interactible.generated.h"

UINTERFACE(Blueprintable, BlueprintType)
class GAMEPROTOTYPE_API UInteractibleInterface : public UInterface
{
	GENERATED_BODY()
};

class GAMEPROTOTYPE_API IInteractibleInterface
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool CanBeInteractedWith(APlayerState* Player);

	virtual bool CanBeInteractedWith_Implementation(APlayerState* Player)
	{
		return true;
	}

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Interact(APlayerState* Player);

	virtual void Interact_Implementation(APlayerState* Player)
	{

	}

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FText GetInteractionNameText() const;

	virtual FText GetInteractionNameText_Implementation() const
	{
		return FText();
	}
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	UInteractionContext* GetInteractionContext(APlayerState* Player);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	bool IsBeingInteractedWith() const;

	bool IsBeingInteractedWith_Implementation() const
	{
		return bIsBeingInteractedWith;
	}



	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetIsBeingInteractedWith(bool Flag);

	virtual	void SetIsBeingInteractedWith_Implementation(bool Flag)
	{
		bIsBeingInteractedWith = Flag;
	}

	bool bIsBeingInteractedWith = false;
};

