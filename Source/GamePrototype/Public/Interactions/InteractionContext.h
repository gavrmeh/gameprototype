#pragma once

#include "UObject/Object.h"

#include "Engine/DataAsset.h"

#include "InteractionContext.generated.h"

UCLASS(Blueprintable, BlueprintType)
class GAMEPROTOTYPE_API UInteractionEntry : public UDataAsset
{
GENERATED_BODY()
public:
	UFUNCTION(BlueprintNativeEvent)
	void OnInteraction(APlayerState* Player);



	virtual void OnInteraction_Implementation(APlayerState* Player) {};
};

UCLASS(Blueprintable, BlueprintType)
class GAMEPROTOTYPE_API UInteractionContext : public UDataAsset
{
GENERATED_BODY()
public:
	TArray<UInteractionEntry*> Entries;
};