#pragma once

#include "EquippableItem.h"
#include "Components/SkeletalMeshComponent.h"
#include "Wearable.generated.h"

UCLASS(Blueprintable, BlueprintType)
class GAMEPROTOTYPE_API AWearable : public AEquippableItem
{
	GENERATED_BODY()
public:
	AWearable() = default;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<USkeletalMesh> SkeletalMesh;
};