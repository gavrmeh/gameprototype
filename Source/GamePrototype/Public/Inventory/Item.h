#pragma once

#include "GameFramework/Actor.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Interfaces/Interactible.h"
#include "Item.generated.h"

UCLASS(Blueprintable, BlueprintType)
class GAMEPROTOTYPE_API AItem : public AActor, public IInteractibleInterface
{
GENERATED_BODY()
public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanItemBeDamaged = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Quantity = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(ClampMin="0"))
	int32 MaxItemsInStack = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UStaticMeshComponent> ItemMesh;

	UFUNCTION(BlueprintCallable)
	bool CanBeStacked() const
	{
		return !bCanItemBeDamaged && MaxItemsInStack > 1;
	}

	UFUNCTION(BlueprintCallable)
	UTexture2D* GetThumbnail() const;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TObjectPtr<UTexture2D> Thumbnail;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText BriefDescription;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText DetailedDescription;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText ItemNameText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ItemName;

	virtual void Interact_Implementation(APlayerState* Player);

	UFUNCTION(BlueprintCallable)
	bool IsInWorld() const
	{
		return bIsInWorld;
	};

	bool bIsInWorld = false;
};