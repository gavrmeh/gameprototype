#pragma once

#include "CoreMinimal.h"

#include "UObject/Object.h"

#include "EquippableItem.h"

#include "EquipmentSlot.generated.h"

UCLASS(BlueprintType, Blueprintable, EditInlineNew)
class GAMEPROTOTYPE_API UEquipmentSlot : public UObject
{
	GENERATED_BODY()
public:
	UEquipmentSlot()
	{

	}
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName SlotName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText SlotNameText;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AEquippableItem* Item = nullptr;

	UFUNCTION(BlueprintCallable)
		bool IsItemEquipped() const
	{
		return Item != nullptr;
	}

};