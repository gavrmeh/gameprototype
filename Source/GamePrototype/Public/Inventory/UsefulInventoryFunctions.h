#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Inventory/Item.h"
#include "Components/ContainerComponent.h"
#include "UsefulInventoryFunctions.generated.h"

UCLASS()
class GAMEPROTOTYPE_API UUsefulInventoryFunctions : public UBlueprintFunctionLibrary
{
GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static void MoveItem(AItem* Item, UContainerComponent* From, UContainerComponent* To)
	{
		if(Item->CanBeStacked())
		{
			AItem* OtherItem = To->GetItemByOtherItemName(Item);
			if (OtherItem)
			{
				if (Item->Quantity != 1)
				{
					Item->Quantity--;
					OtherItem->Quantity++;
				}
				else
				{
					From->Items.Remove(Item);
					OtherItem->Quantity++;
				}
			}
			else
			{
				if (Item->Quantity != 1)
				{
					auto NewItem = NewObject<AItem>(Item->GetOuter(), Item->GetClass());
					To->Items.Add(NewItem);
					Item->Quantity--;
				}
				else
				{
					From->Items.Remove(Item);
					To->Items.Add(Item);
				}
			}
		}
		else
		{
			To->Items.Add(Item);
			From->Items.Remove(Item);
		}

	}
};
