#pragma once

#include "EquippableItem.h"
#include "Character/Damage.h"
#include "Weapon.generated.h"


USTRUCT(BlueprintType)
struct GAMEPROTOTYPE_API FWeaponDamage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BaseDamageValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBasicDamageType* DamageType;
};

UCLASS(BlueprintType, Blueprintable)
class GAMEPROTOTYPE_API AWeapon : public AEquippableItem
{
	GENERATED_BODY()
public:
	AWeapon() = default;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FDamage GetDamage() const;

	virtual FDamage GetDamage_Implementation() const;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	TSet<FWeaponDamage> WeaponDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName WeaponType;
};

GAMEPROTOTYPE_API uint32 GetTypeHash(const FWeaponDamage& Damage);