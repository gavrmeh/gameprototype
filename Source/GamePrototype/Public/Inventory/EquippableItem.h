#pragma once

#include "Item.h"
#include "Character/CharacterAttributes.h"
#include "Engine/SkeletalMesh.h"
#include "EquippableItem.generated.h"

UCLASS(BlueprintType, Blueprintable)
class GAMEPROTOTYPE_API AEquippableItem : public AItem
{
GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FName> UsedSlotNames;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName SocketName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, SaveGame)
	TSet<UAttributeModifier*> Modifiers;

	UPROPERTY(BlueprintReadOnly)
	UCharacterStateComponent* OwningCharacter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsEquipped = false;

	UFUNCTION(BlueprintCallable)
	virtual void OnEquip(UCharacterStateComponent* Character);

	UFUNCTION(BlueprintCallable)
	virtual void OnUnequip();

	virtual bool CanBeInteractedWith_Implementation(APlayerState* Player)
	{
		return OwningCharacter == nullptr;
	}
};
