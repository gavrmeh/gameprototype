// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GamePrototypeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROTOTYPE_API AGamePrototypeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
