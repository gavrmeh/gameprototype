#include "Components/AttributesComponent.h"

void UCharacterStateComponent::BeginPlay()
{
	Super::BeginPlay();
	OnSetParentPawn(Cast<APawn>(GetOwner()));
	Container = reinterpret_cast<UContainerComponent*>(GetOwner()->GetComponentByClass(UContainerComponent::StaticClass()));
}

void UCharacterStateComponent::OnSetParentPawn(APawn* NewPawn)
{
	check(NewPawn);
	OwningPawn = NewPawn;
}

void UCharacterStateComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* TickFunction)
{
	for (auto Attr : Attributes)
	{
		if (Attr->bShouldTick)
		{
			Attr->Tick(DeltaTime, this);
		}
	}
	for (auto StatusEffect : StatusEffects)
	{
		if (StatusEffect->bShouldTick)
		{
			StatusEffect->TickStatusEffect(DeltaTime);
		}
	}
}