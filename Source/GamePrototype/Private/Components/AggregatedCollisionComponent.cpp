#include "Components/AggregatedCollisionComponent.h"

UAggregatedCollisionComponent::UAggregatedCollisionComponent()
{
}

void UAggregatedCollisionComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UAggregatedCollisionComponent::AddCollisionComponent(UPrimitiveComponent* Component)
{
	if (!CollisionComponents.Contains(Component))
	{
		CollisionComponents.Add(Component);
	}
}

bool UAggregatedCollisionComponent::RemoveCollisionComponent(UPrimitiveComponent* Component)
{
	return (bool)CollisionComponents.Remove(Component);
}

FBoxSphereBounds UAggregatedCollisionComponent::CalcBounds(const FTransform& LocalToWorld) const
{
	FBoxSphereBounds Result;
	for (UPrimitiveComponent* CollisionComponent : CollisionComponents)
	{
		Result = Result + CollisionComponent->CalcBounds(LocalToWorld);
	}
	return Result;
}

bool UAggregatedCollisionComponent::MoveComponentImpl(const FVector& Delta, const FQuat& NewRotation, bool bSweep, FHitResult* OutHit, EMoveComponentFlags MoveFlags, ETeleportType Teleport)
{
	for (UPrimitiveComponent* CollisionPrimitiveComponent : CollisionComponents)
	{
		if (CollisionPrimitiveComponent)
		{
			FCollisionShape Shape = CollisionPrimitiveComponent->GetCollisionShape();
			FHitResult Hit;
			FCollisionQueryParams CollisionQueryParams;
			CollisionQueryParams.AddIgnoredActor(GetAttachmentRootActor());
			GetWorld()->SweepSingleByChannel(Hit,
				GetComponentLocation(),
				GetComponentLocation() + Delta,
				NewRotation, GetCollisionObjectType(),
				Shape,
				CollisionQueryParams);
			if (Hit.IsValidBlockingHit())
			{
				*OutHit = Hit;
				return false;
			}
		}
	}
	return true;
}
