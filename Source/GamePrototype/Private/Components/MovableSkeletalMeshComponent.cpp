#include "Components/MovableSkeletalMeshComponent.h"
#include "Engine/SkeletalMeshSocket.h"
UMovableSkeletalMeshComponent::UMovableSkeletalMeshComponent()
{

}

void UMovableSkeletalMeshComponent::BeginPlay()
{
	Super::BeginPlay();
}

bool UMovableSkeletalMeshComponent::MoveComponentImpl(const FVector& Delta, const FQuat& NewRotation, bool bSweep, FHitResult* OutHit, EMoveComponentFlags MoveFlags, ETeleportType Teleport)
{
	for(UPrimitiveComponent* CollisionPrimitiveComponent : CollisionComponents)
	{
		if(CollisionPrimitiveComponent)
		{
			FCollisionShape Shape = CollisionPrimitiveComponent->GetCollisionShape();
			FHitResult Hit;
			FCollisionQueryParams CollisionQueryParams;
			CollisionQueryParams.AddIgnoredActor(GetAttachmentRootActor());
			GetWorld()->SweepSingleByChannel(Hit, 
				GetComponentLocation(), 
				GetComponentLocation() + Delta,
				NewRotation, GetCollisionObjectType(), 
				Shape, 
				CollisionQueryParams);
			if (Hit.IsValidBlockingHit())
			{
				*OutHit = Hit;
				return false;
			}
		}
	}
	return true;
}
