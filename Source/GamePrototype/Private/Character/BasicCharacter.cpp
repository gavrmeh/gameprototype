// Fill out your copyright notice in the Description page of Project Settings.


#include "Character/BasicCharacter.h"
#include "EnhancedInputComponent.h"
#include "InputMappingContext.h"
#include "EnhancedInputSubsystems.h"
#include "Components/SphereComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/BoxComponent.h"

// Sets default values
ABasicCharacter::ABasicCharacter()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	ConstructorHelpers::FObjectFinder<UDataTable> CharacterAttackAnimTableHelper(TEXT("UDataTable'/Game/Data/CharacterAttackAnimationsTable.CharacterAttackAnimationsTable'"));
	if (CharacterAttackAnimTableHelper.Succeeded())
	{
		CharacterAttackAnimTable = CharacterAttackAnimTableHelper.Object;
	}
	PrimaryActorTick.bCanEverTick = true;

	Inventory = CreateDefaultSubobject<UContainerComponent>("InventoryComponent");
	CharacterState = CreateDefaultSubobject<UCharacterStateComponent>("CharacterState");
	//AddCom
}

void ABasicCharacter::PawnClientRestart()
{
	Super::PawnClientRestart();
	if(APlayerController* PlayerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->ClearAllMappings();
			if(InputMapping)
			{
				Subsystem->AddMappingContext(InputMapping, 0);
			}
		}
	};
}

// Called when the game starts or when spawned
void ABasicCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasicCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ABasicCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	APawn::SetupPlayerInputComponent(PlayerInputComponent);
	UEnhancedInputComponent* PlayerEnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent);
}

void ABasicCharacter::ReceiveDamage_Implementation(const FDamage& Damage)
{
	UCharacterStateAttribute* HealthAttr = Cast<UCharacterStateAttribute>(CharacterState->GetAttributeByName("Health"));
	for (auto StatusEffect : CharacterState->StatusEffects)
	{
		StatusEffect->OnTakeDamage(Damage);
	}
	if(HealthAttr)
	{
		UCharacterStateComponent* InstigatorState = 
			reinterpret_cast<UCharacterStateComponent*>(Damage.DamageInstigator->GetComponentByClass(
				UCharacterStateComponent::StaticClass()
			));
		for (auto DamageComponent : Damage.DamageComponents)
		{
			float ReceivedDamage = DamageComponent.DamageType->CalculateIncomingDamage(DamageComponent.Value, CharacterState, InstigatorState);
			HealthAttr->CurrentValue -= ReceivedDamage;
		}
	}
}
/*
void ABasicCharacter::UpdateCharacterAttackAnimations_Implementation(AWeapon* Weapon)
{
	USkeletalMeshComponent* RootSkeletalMesh = GetRootSkeletalMesh_BP();
	if(RootSkeletalMesh)
	{
		TArray<FCharacterAttackAnimTableRow*> Rows;
		CharacterAttackAnimTable->GetAllRows(TEXT("GetAllRows"), Rows);
	
		FName WeaponType = Weapon ? Weapon->WeaponType : FName();

		for (auto Row : Rows)
		{	
			if ((Row->SkeletonClassName == RootSkeletalMesh->SkeletalMesh->Skeleton->GetFName()) && (Row->WeaponType == WeaponType))
			{
				AttackAnimations = Row->Animation.Get();
			}
		}
	}
}*/
