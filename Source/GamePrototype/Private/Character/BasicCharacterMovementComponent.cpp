// Fill out your copyright notice in the Description page of Project Settings.

/*
#include "Character/BasicCharacterMovementComponent.h"
#include "Character/BasicCharacter.h"
#include "DrawDebugHelpers.h"
float UBasicCharacterMovementComponent::GetMaxSpeed() const
{
	return MaxSpeed * (Character->bIsRunning ? RunningSpeedMultiplyer : 1) * (Character->bIsCrouching ? CrouchingSpeedMultiplyer : 1);
}
void UBasicCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	LegData.Reset();

	for (auto Leg : Legs)
	{
		FCharacterLegData Data;
		Data.Leg = Leg;
		LegData.Push(Data);
	}
}
void UBasicCharacterMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (!PawnOwner || !UpdatedComponent || ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	UpdateFloor();

	Character->RootSkeleton->GetCollisionShape();
	ChooseMovementMode();
	if (PawnOwner->IsLocallyControlled())
	{
		RotateAuthority();
		UpdateVelocityAuthority(DeltaTime);
	}
	else
	{

	}
	Move(DeltaTime);

}

void UBasicCharacterMovementComponent::SetUpdatedComponent(USceneComponent* NewComponent)
{
	Super::SetUpdatedComponent(NewComponent);
	Character = Cast<ABasicCharacter>(NewComponent->GetAttachmentRootActor());
	check(Character);
	check(!MainCollisionSocketName.IsNone())
	LegData.Reset();
	for (auto& Leg : Legs)
	{
		FCharacterLegData Data;
		Data.Leg = Leg;
		Data.BoneIndex = Character->SkeletalMesh->GetBoneIndex(Data.Leg.LegBone);
		LegData.Push(Data);
	}
	
}

FFloorSearchResult UBasicCharacterMovementComponent::FindFloor(const UPrimitiveComponent* Primitive)
{
	FFloorSearchResult Result;
	if (Primitive)
	{
		FVector Position = Primitive->GetComponentLocation();
		FHitResult InitialHitResult;
		FCollisionShape Shape = reinterpret_cast<UPrimitiveComponent*>(UpdatedComponent.Get())->GetCollisionShape();
		FCollisionQueryParams CollisionQueryParams;
		CollisionQueryParams.AddIgnoredActor(UpdatedComponent->GetAttachmentRootActor());
		FVector SweepEnd;
		SweepEnd = Position + FVector(0,0,-FloorCheckDistance);
		UpdatedComponent->GetWorld()->SweepSingleByChannel(InitialHitResult,Position, SweepEnd, UpdatedComponent->GetComponentQuat(), UpdatedComponent->GetCollisionObjectType(), Shape, CollisionQueryParams);
		if (InitialHitResult.IsValidBlockingHit())
		{
			Result = FFloorSearchResult(InitialHitResult);
		}
		else
		{
			TArray<FHitResult> ExtendedSearchResult;
			UpdatedComponent->GetWorld()->SweepMultiByChannel(ExtendedSearchResult, Position, SweepEnd, UpdatedComponent->GetComponentQuat(), UpdatedComponent->GetCollisionObjectType(), Shape, CollisionQueryParams);
			Result = FFloorSearchResult(ExtendedSearchResult);
		}
	}
	return Result;
}

void UBasicCharacterMovementComponent::UpdateFloor()
{
	int32 NumStandingLegs = 0;
	bool bIsRightSideStanding = false;
	bool bIsLeftSideStanding = false;
	for (uint32 i = 0; i < LegData.Num(); i++)
	{
		TraceLeg(LegData[i]);
		if (LegData[i].Floor.bIsValidFloor)
		{
			NumStandingLegs++;
			if (LegData[i].Leg.LegSide == ELegSide::Left)
			{
				bIsLeftSideStanding = true;
			}
			else
			{
				bIsRightSideStanding = true;
			}
		}
	}
	bIsOnFloor = NumStandingLegs >= MinNumLegsForStability && bIsRightSideStanding && bIsLeftSideStanding; 
}

bool UBasicCharacterMovementComponent::IsStandingOnFloor()
{
	return bIsOnFloor;
}

void UBasicCharacterMovementComponent::Move(float DeltaTime)
{
	switch (MovementMode)
	{
	case EMovementMode::MOVE_Falling:
		Fall(DeltaTime);
	break;
	case EMovementMode::MOVE_Flying:
		Fly(DeltaTime);
	break;
	case EMovementMode::MOVE_Walking:
		Walk(DeltaTime);
	break;
	case EMovementMode::MOVE_NavWalking:
		WalkNavMesh(DeltaTime);
	break;
	case EMovementMode::MOVE_Swimming:
		Swim(DeltaTime);
	default:
		break;
	}
}

void UBasicCharacterMovementComponent::Walk(float DeltaTime)
{
	
	if (bShouldJump)
	{
		FHitResult JumpMovementHitResult;
		SafeMoveUpdatedComponent(Velocity*DeltaTime, UpdatedComponent->GetComponentRotation(), true, JumpMovementHitResult);
		bShouldJump = false;
	}
	else
	{
		FVector Movement = GetMovementAlongRamp(Velocity, FloorSearchResult) *DeltaTime;
		GEngine->AddOnScreenDebugMessage(INDEX_NONE,0, FColor::White, FString::Printf(TEXT("Velocity = %f %f %f"), Velocity.X, Velocity.Y, Velocity.Z));
		FHitResult InitialMovementHitResult;
		SafeMoveUpdatedComponent(Movement, UpdatedComponent->GetComponentRotation(), true, InitialMovementHitResult);
		bool bHasSteppedUp = false;
		FVector RemainingMovement = Movement*(1 - InitialMovementHitResult.Time);
		if (InitialMovementHitResult.IsValidBlockingHit())
		{
			HandleImpact(InitialMovementHitResult, Movement);
			if(IsWalkableFloor(InitialMovementHitResult))
			{
				float RemainingMovementSize = RemainingMovement.Size();
				RemainingMovement = GetMovementAlongRamp(RemainingMovement, FFloorSearchResult(InitialMovementHitResult));
				RemainingMovement.Normalize();
				RemainingMovement*=RemainingMovementSize;
				FHitResult RemainingMovementHit;
				SafeMoveUpdatedComponent(RemainingMovement, UpdatedComponent->GetComponentRotation(), true, RemainingMovementHit);
				HandleImpact(RemainingMovementHit, RemainingMovement);
			}
			else
			{
				MoveAlongWall(RemainingMovement, InitialMovementHitResult);
			}
		}
		bHasSteppedUp = StepUp(RemainingMovement, InitialMovementHitResult);
		UpdateFloor();

		if (FloorSearchResult.bIsValidFloor)
		{
			if (!bIsZRotationFixed)
			{
				SetRotationFromFloor();
			}
		}

		UpdateDistanceFromFloor();

		/*if (!bHasSteppedUp && ShouldSlideOffFloor())
		{
			FVector HitPointToComponentLocation = UpdatedComponent->GetComponentLocation() - FloorSearchResult.FloorHitPosition;
			FVector SlideOffNormal = HitPointToComponentLocation;
			SlideOffNormal.Normalize();
			FVector SlideOffMovement = FVector::VectorPlaneProject(FVector(HitPointToComponentLocation.X, HitPointToComponentLocation.Y, 0), SlideOffNormal);
			SlideOffMovement.Normalize();
			SlideOffMovement*=MaxSpeed*DeltaTime;
			FHitResult SlideOffHit;
			bool bHasSlidOff = SafeMoveUpdatedComponent(SlideOffMovement, UpdatedComponent->GetComponentRotation(), true, SlideOffHit);
			int k = 0;
		}*/
/*	}
}

void UBasicCharacterMovementComponent::Fall(float DeltaTime)
{
	FHitResult Hit;
	FVector DesiredMovement = Velocity*DeltaTime;
	SafeMoveUpdatedComponent(DesiredMovement, UpdatedComponent->GetComponentRotation(), true, Hit);
	if (Hit.IsValidBlockingHit())
	{
		SlideAlongSurface(DesiredMovement, 1-Hit.Time, Hit.Normal, Hit);
		HandleImpact(Hit, DesiredMovement);
	}
}

void UBasicCharacterMovementComponent::Swim(float DeltaTime)
{
}

void UBasicCharacterMovementComponent::Fly(float DeltaTime)
{
}

void UBasicCharacterMovementComponent::WalkNavMesh(float DeltaTime)
{
}

FFloorSearchResult::FFloorSearchResult(const FHitResult& Hit)
{
	if(Hit.IsValidBlockingHit())
	{
		Normal = Hit.Normal;
		FloorHitPosition = Hit.ImpactPoint;
		PhysicalMaterials.Add(Hit.PhysMaterial);
		Components.Add(Hit.Component);
		ImpactNormal = Hit.ImpactNormal;
		float WalkableZ = WalkableFloorZ;

		WalkableZ = Hit.GetComponent()->GetWalkableSlopeOverride().ModifyWalkableFloorZ(WalkableZ);
		bIsValidFloor = Normal.Z > WalkableZ;
	}
}

FFloorSearchResult::FFloorSearchResult(const TArray<FHitResult>& Hits)
{
	if(!Hits.IsEmpty())
	{
		PhysicalMaterials.Reserve(Hits.Num());
		Components.Reserve(Hits.Num());
		for (const FHitResult& Hit : Hits)
		{
			if(Hit.IsValidBlockingHit())
			{
				Normal += Hit.Normal;
				FloorHitPosition+=Hit.ImpactPoint;
				PhysicalMaterials.Add(Hit.PhysMaterial);
				Components.Add(Hit.Component);
			}
		}
		if(!FloorHitPosition.ContainsNaN())
		{
			FloorHitPosition/=Hits.Num();
		}
		if(!Normal.IsNearlyZero())
		{
			Normal.Normalize();
			bIsValidFloor = Normal.Z > WalkableFloorZ;
		}
	}
}

void UBasicCharacterMovementComponent::UpdateVelocityAuthority(float DeltaTime)
{
	if (MovementMode == EMovementMode::MOVE_Falling)
	{
		Velocity += FVector(0,0,UpdatedComponent->GetWorld()->GetGravityZ())*DeltaTime;
	}
	else
	{
		FVector Input = ConsumeInputVector();

		int32 JumpCount = Character->CheckJumpInput();

		if(!Input.IsZero())
		{
			float Speed = Velocity.Size();
			Input.Normalize();
			Velocity = (Input*(Speed + Acceleration*DeltaTime)).GetClampedToSize(0, GetMaxSpeed());

		}
		else
		{
			if (!Velocity.IsNearlyZero(0.5))
			{
				float CurrentSpeed = Velocity.Size();
				CurrentSpeed -= Acceleration*DeltaTime;
				Velocity.Normalize();
				Velocity*=CurrentSpeed;
			}
			else
			{
				Velocity = FVector::ZeroVector;
			}
		}
		if (JumpCount)
		{
			float InitialJumpVelocity = FMath::Sqrt(-2*GetWorld()->GetGravityZ()*JumpHeight);
			Velocity += FVector(0,0,InitialJumpVelocity);
			bShouldJump = true;
		}
	}
}

void UBasicCharacterMovementComponent::UpdatedVelocitySimulatedProxy(float DeltaTime)
{
}

void UBasicCharacterMovementComponent::ChooseMovementMode()
{
	
	if (IsStandingOnFloor())
	{
		if (PawnOwner->IsPlayerControlled())
		{
			MovementMode = EMovementMode::MOVE_Walking;
		}
		else
		{
			MovementMode = EMovementMode::MOVE_NavWalking;
		}
	}
	else
	{
		MovementMode = EMovementMode::MOVE_Falling;
	}
	//TODO: Add checks for flying/swimming
}

bool UBasicCharacterMovementComponent::StepUp(const FVector& DesiredMovement, const FHitResult& Hit)
{
	FScopedMovementUpdate PreStepUpMovement(UpdatedComponent);
	//Try to step up if we hit something
	if (Hit.Location.Z > UpdatedComponent->GetComponentLocation().Z + UpdatedComponent->Bounds.BoxExtent.Z * 0.75)
	{
		return false;
	}
	if(Hit.IsValidBlockingHit())
	{
		FHitResult StepUpHitResult;
		FHitResult MoveForwardHitResult;
		bool bHasMovedUp = MoveUpdatedComponent(FVector(0,0,MaxStepUpHeight),UpdatedComponent->GetComponentRotation(), true, &StepUpHitResult);
		if (!bHasMovedUp)
		{
			PreStepUpMovement.RevertMove();
			return false;
		}
		bool bHasMovedForward = MoveUpdatedComponent(DesiredMovement, UpdatedComponent->GetComponentRotation(), true, &MoveForwardHitResult);
		if (MoveForwardHitResult.bBlockingHit)
		{

		}
		
		FVector RemainingMovement = DesiredMovement*(1 - MoveForwardHitResult.Time);
		/*if (ShouldSlideAlongWall(RemainingMovement, MoveForwardHitResult))
		{
			bool bHasMovedAlongWall = MoveAlongWall(RemainingMovement, MoveForwardHitResult);
			if (!bHasMovedAlongWall)
			{
				PreStepUpMovement.RevertMove();
				return false;
			}
		};*/
/*		if (!bHasMovedForward)
		{
			PreStepUpMovement.RevertMove();
			return false;
		}
		MoveUpdatedComponent(FVector(0,0,-MaxStepUpHeight), UpdatedComponent->GetComponentRotation(), true, &StepUpHitResult);
		if (!IsWalkableFloor(StepUpHitResult))
		{
			PreStepUpMovement.RevertMove();
			return false;
		}
	}
	/*FHitResult StepDownResult;
	bool bHasMovedDown = MoveUpdatedComponent(FVector(0,0,-MaxStepUpHeight), UpdatedComponent->GetComponentRotation(),true, &StepDownResult);

	if (!StepDownResult.IsValidBlockingHit())
	{
		PreStepUpMovement.RevertMove();
		return false;
	}
	*/
/*	return true;
}

void UBasicCharacterMovementComponent::AdjustCollisionShape(FCollisionShape& Shape)
{
	if (Shape.IsCapsule())
	{
		Shape.Capsule.Radius*=0.9;
	}
	else if (Shape.IsSphere())
	{
		Shape.Sphere.Radius*=0.9;
	}
	else if (Shape.IsBox())
	{
		Shape.Box.HalfExtentX*=0.9;
		Shape.Box.HalfExtentY*=0.9;
	}
}

bool UBasicCharacterMovementComponent::IsWalkableFloor(const FHitResult& Hit)
{
	float WalkableZ = WalkableFloorZ;
	if (Hit.GetComponent())
	{
		WalkableZ = Hit.GetComponent()->GetWalkableSlopeOverride().ModifyWalkableFloorZ(WalkableZ);
	}
	if (Hit.Normal.Z > WalkableZ)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool UBasicCharacterMovementComponent::ShouldSlideAlongWall(const FVector& MovementVector, const FHitResult& WallHit)
{
	
	return !MovementVector.IsNearlyZero() && FMath::Abs(MovementVector.CosineAngle2D(WallHit.Normal)) < FMath::Cos(FMath::DegreesToRadians(MaxSlidingAngle));
}

void UBasicCharacterMovementComponent::Perch()
{
}

bool UBasicCharacterMovementComponent::MoveAlongWall(const FVector& DesiredMovement, const FHitResult& WallHit)
{
	float DesiredMovementDistance =DesiredMovement.Size();

	FVector Movement = FVector::VectorPlaneProject(FVector::VectorPlaneProject(DesiredMovement, WallHit.Normal), FloorSearchResult.Normal);
	Movement.Normalize();
	Movement*=DesiredMovementDistance;
	FHitResult MovementHitResult;
	return SafeMoveUpdatedComponent(Movement, UpdatedComponent->GetComponentRotation(), true, MovementHitResult);
}

bool UBasicCharacterMovementComponent::ShouldSlideOffFloor()
{
	UPrimitiveComponent* PrimitiveUpdatedComponent = reinterpret_cast<UPrimitiveComponent*>(UpdatedComponent.Get());
	FCollisionShape Shape = PrimitiveUpdatedComponent->GetCollisionShape();
	if (Shape.IsBox())
	{
		return false;
	}
	else if (Shape.IsCapsule())
	{
		float Distance = FMath::Abs(UpdatedComponent->GetComponentLocation().Z - FloorSearchResult.FloorHitPosition.Z);
		float SlideOffDistance = Shape.Capsule.HalfHeight - (1 - SlideOffShapeRadiusPercentage) * Shape.Capsule.Radius;
		GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::White, FString::Printf(TEXT("Distance = %f, SlideOffDistance = %f"), Distance, SlideOffDistance));
		return Distance < SlideOffDistance;
	}
	else 
	{
		return FMath::Abs(UpdatedComponent->GetComponentLocation().Z - FloorSearchResult.FloorHitPosition.Z) < Shape.Sphere.Radius - (1 - SlideOffShapeRadiusPercentage * Shape.Sphere.Radius);
	}
	
}

bool UBasicCharacterMovementComponent::IsWithingEdgeTolerance(const FFloorSearchResult& Floor) const
{
	return false;
}

void UBasicCharacterMovementComponent::HandleImpact(const FHitResult& Hit, const FVector& MovementDir)
{

	if (Hit.GetComponent() && Character->bEnablePhysicsInteractions)
	{	
		if(Hit.GetComponent()->IsSimulatingPhysics(Hit.BoneName))
		{
			Hit.GetComponent()->AddForceAtLocation(MovementDir, Hit.ImpactPoint, Hit.BoneName);
		}
	}
}

FVector UBasicCharacterMovementComponent::GetMovementAlongRamp(const FVector& InMovement, const FFloorSearchResult& Surface)
{
	if (InMovement.IsNearlyZero())
	{
		return FVector::ZeroVector;
	}
	if (Surface.ImpactNormal.SizeSquared2D() < KINDA_SMALL_NUMBER)
	{
		return FVector(InMovement.X, InMovement.Y, 0);
	}
	else
	{
		return FVector(InMovement.X, InMovement.Y, -Surface.ImpactNormal | Surface.Normal / Surface.Normal.Z);
	}
}

float UBasicCharacterMovementComponent::GetUpdatedComponentLowerPointZ() const
{
	
	return UpdatedComponent->GetComponentLocation().Z - reinterpret_cast<UPrimitiveComponent*>(UpdatedComponent.Get())->Bounds.BoxExtent.Z/2;
}

void UBasicCharacterMovementComponent::RotateAuthority()
{
	FRotator ComponentRotation = UpdatedComponent->GetComponentRotation();
	ComponentRotation.Yaw += Rotation.X;
	UpdatedComponent->SetWorldRotation(ComponentRotation);
	Rotation = FVector2D::ZeroVector;
}

void UBasicCharacterMovementComponent::SetRotationFromFloor()
{
	
	FVector DeltaVec = FloorSearchResult.FloorHitPosition - UpdatedComponent->GetComponentLocation();

	UpdatedComponent->MoveComponent(DeltaVec, UpdatedComponent->GetComponentRotation(), false);

	FVector Forward = Character->GetActorForwardVector();
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1, FColor::Red, Forward.ToString());
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, -1, FColor::Red, FloorSearchResult.ImpactNormal.ToString());
	DrawDebugLine(GetWorld(), FloorSearchResult.FloorHitPosition, FloorSearchResult.FloorHitPosition + Forward * 50, FColor::Blue);
	Forward = FVector::VectorPlaneProject(Forward, FloorSearchResult.ImpactNormal);
		
	DrawDebugLine(GetWorld(), FloorSearchResult.FloorHitPosition, FloorSearchResult.FloorHitPosition + FloorSearchResult.ImpactNormal * 50, FColor::Red);
	DrawDebugLine(GetWorld(), FloorSearchResult.FloorHitPosition, FloorSearchResult.FloorHitPosition + Forward * 50, FColor::Green);

	UpdatedComponent->SetWorldRotation(Forward.Rotation());

	UpdatedComponent->MoveComponent(-DeltaVec, UpdatedComponent->GetComponentRotation(), false);
	//FTransform Transform

}

void UBasicCharacterMovementComponent::UpdateDistanceFromFloor()
{
	/*if (FloorSearchResult.bIsValidFloor)
	{
		FBoxSphereBounds CurrentBounds = UpdatedComponent->CalcBounds(UpdatedComponent->GetComponentToWorld());
		FVector DeltaVec = UpdatedComponent->GetComponentLocation() - FloorSearchResult.FloorHitPosition;
		
		float Distance = DeltaVec.Size() - CurrentBounds.BoxExtent.Z;
		if (Distance > MaxDistanceToFloor)
		{
			DeltaVec = -DeltaVec.GetSafeNormal()*(Distance - MaxDistanceToFloor);
			UpdatedComponent->MoveComponent(DeltaVec, UpdatedComponent->GetComponentRotation(), false);
		}
		else if (Distance < 0)
		{
			DeltaVec = -DeltaVec*1.05;
			UpdatedComponent->MoveComponent(DeltaVec, UpdatedComponent->GetComponentRotation(), false);
		}
	}*/
/*}

void UBasicCharacterMovementComponent::TraceLeg(FCharacterLegData& Leg)
{
	FHitResult HitResult;
	FCollisionQueryParams CollisionQueryParams;
	FVector Start = Character->SkeletalMesh->GetBoneTransform(Leg.BoneIndex).GetLocation();
	FVector End = Start;
	End.Z -= LegTraceDistance;
	CollisionQueryParams.AddIgnoredActor(Character);
	GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, UpdatedComponent->GetCollisionObjectType(), CollisionQueryParams);
	if (HitResult.IsValidBlockingHit() && HitResult.Distance < MaxDistanceToFloor)
	{
		Leg.Floor = FFloorSearchResult(HitResult);
	}
	else
	{
		Leg.Floor = FFloorSearchResult();
	}
}

FVector UBasicCharacterMovementComponent::GetFrontTracePos() const
{
	FVector Result = FVector::ZeroVector;

	for (auto& Leg : LegData)
	{

	}

	return FVector();
}

*/