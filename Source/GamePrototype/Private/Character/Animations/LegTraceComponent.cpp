/*#include "Character/Animations/LegTraceComponent.h"
#include "DrawDebugHelpers.h"
#include "Character/BasicCharacter.h"
void ULegTraceComponent::UpdateLeg(float DeltaTime)
{
	FVector PreviousPosition = GetComponentLocation();
	
	if (bIsPlanted && LegManager->CanLegStep(this))
	{
		bIsPlanted = false;
		StepAlpha = 0;
		if (!IsInRestingPosition())
		{
			bIsCatchingUp = true;
		}
	}
	if (!bIsPlanted)
	{
		StepAlpha += DeltaTime/LegManager->StepTime;
		FVector Position;
		if (bIsCatchingUp)
		{
			
			FVector PreviousLocation = PreviousComponentToWorld.GetLocation();
			FVector PreviousDirection = PreviousComponentToWorld.GetRotation().Vector();
			FTransform CurrentComponentToWorld = LegManager->Root->GetComponentToWorld();
			FVector TargetLocation = CurrentComponentToWorld.GetLocation();
			FVector CurrentPosition = FMath::CubicInterp(
				PreviousLocation,
				PreviousDirection,
				TargetLocation,
				-CurrentComponentToWorld.GetRotation().Vector(),
				StepAlpha/LegManager->CatchUpStepPart);
			FQuat CurrentRotation = PreviousComponentToWorld.GetRotation() + (CurrentComponentToWorld.GetRotation() - PreviousComponentToWorld.GetRotation())*StepAlpha/LegManager->CatchUpStepPart;

			FTransform CurrentTransform(CurrentRotation, CurrentPosition, CurrentComponentToWorld.GetScale3D());

			Position = CurrentTransform.TransformPosition(LocalFootRestingPosition);


			if (StepAlpha > LegManager->CatchUpStepPart)
			{
				bIsCatchingUp = false;
				DrawDebugSphere(GetWorld(), Position, 16, 10, FColor::Red,false, 10);
			}
		}
		else
		{
			FVector CurrentPosition = GetComponentLocation();
			GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Red, FString::Printf(TEXT("Position = %f %f %f"), CurrentPosition.X, CurrentPosition.Y, CurrentPosition.Z));
			Position = LegManager->Root->GetComponentToWorld().TransformPosition(LocalFootRestingPosition);
		}

		Position.Z += StepHeightCurve->GetFloatValue(StepAlpha)*StepHeight;
		SetWorldTransform(FTransform(GetComponentRotation(), Position, GetComponentScale()));
		if (StepAlpha > 1)
		{
			bIsPlanted = true;
			PreviousComponentToWorld = LegManager->Root->GetComponentToWorld();
			bIsStepping = false;
			StepAlpha = 0;
			bHasStepped = true;
			bIsSteppingFromBack = true;
			bIsStepTargetFixed = false;
		}
	}
	if (bShouldDebugDraw)
	{
		//DrawDebugCone(GetWorld(), PreviousStepPosition, FVector::UpVector, 10, PI/10,PI/5,10, FColor::Magenta);
		//DrawDebugSphere(GetWorld(), GetComponentLocation(), 1, 10, FColor::White, false, 10);
		//DrawDebugBox(GetWorld(), StepTarget, FVector(5,5,5), FColor::Green);
		//DrawDebugSphere(GetWorld(), GetAttachmentRootActor()->GetRootComponent()->GetComponentToWorld().TransformPosition(LocalFootRestingPosition), 16, 10, FColor::Red);
		FVector LineStart = PreviousPosition;
		LineStart.Z -= StepHeightCurve->GetFloatValue(StepAlpha - DeltaTime/TotalStepTime)*StepHeight;
		FVector LineEnd = GetComponentLocation();
		LineEnd.Z -= StepHeightCurve->GetFloatValue(StepAlpha)*StepHeight;
		DrawDebugLine(GetWorld(), LineStart, LineEnd, FColor::White, false, 10);
	}
}

void ULegTraceComponent::BeginPlay()
{
	Super::BeginPlay();
	LegManager = reinterpret_cast<ULegManagerComponent*>(GetAttachmentRootActor()->GetComponentByClass(ULegManagerComponent::StaticClass()));
	PreviousStepPosition = GetComponentLocation();
	LocalFootRestingPosition = PreviousStepPosition - GetAttachmentRootActor()->GetRootComponent()->GetComponentLocation();
	
	SetAbsolute(true, true, true);
	SetComponentToWorld(FTransform(PreviousStepPosition));
	StepTarget = PreviousStepPosition;
	


	MovementComponent = reinterpret_cast<UBasicCharacterMovementComponent*>(GetAttachmentRootActor()->GetComponentByClass(UBasicCharacterMovementComponent::StaticClass()));
}

void ULegTraceComponent::StartStep()
{
	bCanReplant = false;
	bIsPlanted = false;
	StepAlpha = 0;
	UpdateStepTarget();
}

bool ULegTraceComponent::IsInRestingPosition() const
{
	FVector RestingPosition = GetAttachmentRootActor()->GetRootComponent()->GetComponentToWorld().TransformPosition(LocalFootRestingPosition);
	return (RestingPosition - GetComponentLocation()).IsNearlyZero(2);

}
void ULegTraceComponent::UpdateStepTarget()
{
	//We start the ray from where our foot default position is

	if (LegManager->LegMovementState == ELegMovementState::Standing)
	{
		StepTarget = GetAttachmentRootActor()->GetRootComponent()->GetComponentToWorld().TransformPosition(LocalFootRestingPosition);
	}
	else
	{
		if (LegManager->DirectionDeltaAngle > 1.5)
		{
			bIsStepTargetFixed = true;
			switch (LegSide)
			{
			case ELegSide::Left:
				StepTarget = LegManager->Root->GetComponentToWorld().TransformPosition(LocalFootRestingPosition)+LegManager->Root->GetComponentRotation().Vector()*(1-StepAlpha)*StepLength;
			break;
			case ELegSide::Right:
				StepTarget = LegManager->Root->GetComponentToWorld().TransformPosition(LocalFootRestingPosition);
			break;
			};
			return;
		}
		if (LegManager->DirectionDeltaAngle < -1.5)
		{
			bIsStepTargetFixed = true;
			switch (LegSide)
			{
			case ELegSide::Left:
				StepTarget = LegManager->Root->GetComponentToWorld().TransformPosition(LocalFootRestingPosition);
			break;
			case ELegSide::Right:
				StepTarget = LegManager->Root->GetComponentToWorld().TransformPosition(LocalFootRestingPosition) + LegManager->Root->GetComponentRotation().Vector() * (1 - StepAlpha) * StepLength;
			break;
			};
			return;
		}
		if (bIsStepTargetFixed)
		{
			return;
		}

		FVector RayStart = GetAttachmentRootActor()->GetRootComponent()->GetComponentToWorld().TransformPosition(LocalFootRestingPosition);
		RayStart.Z += 10;
		//Offset ray start a bit so that we do not trace into other actors side

		FVector Direction = MovementComponent->GetVelocity();
		Direction.Normalize();
		Direction*=StepLength*(1-StepAlpha);

		FVector RayEnd = RayStart + Direction;


		FHitResult StepTargetSearchResult;
		FCollisionQueryParams StepTargetSearchCollisionQueryParams;
		StepTargetSearchCollisionQueryParams.AddIgnoredActor(GetAttachmentRootActor());
		GetWorld()->LineTraceSingleByChannel(StepTargetSearchResult,
			RayStart,
			RayEnd,
			GetAttachmentRootActor()->GetRootComponent()->GetCollisionObjectType(),
			StepTargetSearchCollisionQueryParams);
		if (!StepTargetSearchResult.IsValidBlockingHit())
		{
			FVector SecondarySearchRayEnd = RayEnd;
			SecondarySearchRayEnd.Z -= StepLength;
			GetWorld()->LineTraceSingleByChannel(StepTargetSearchResult,
				RayEnd,
				SecondarySearchRayEnd,
				GetAttachmentRootActor()->GetRootComponent()->GetCollisionObjectType(),
				StepTargetSearchCollisionQueryParams);
		}
		if (StepTargetSearchResult.IsValidBlockingHit())
		{
			StepTarget = StepTargetSearchResult.ImpactPoint;
			
			GEngine->AddOnScreenDebugMessage(INDEX_NONE, 0, FColor::Red, FString::Printf(TEXT("StepTarget = %f %f %f"),StepTarget.X, StepTarget.Y, StepTarget.Z));
		}

	}
}
*/
