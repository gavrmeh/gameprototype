#include "Character/Animations/LegManagerComponent.h"
#include "Character/BasicCharacter.h"
#include "DrawDebugHelpers.h"
/*void ULegManagerComponent::BeginPlay()
{
	Super::BeginPlay();
	MovementComponent = GetOwner<ABasicCharacter>()->MovementComponent;
	Root = GetOwner()->GetRootComponent();
	TrajectoryCacheDeltaTime = StepTime/NumCachedTrajectoryPoints;
	CachedTrajectory = TCircularBuffer<FTransform>(NumCachedTrajectoryPoints);
}

void ULegManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	TimeSinceLastTrajectoryCache += DeltaTime;
	if (TimeSinceLastTrajectoryCache > TrajectoryCacheDeltaTime)
	{
		TrajectoryStartIndex = CachedTrajectory.GetPreviousIndex(TrajectoryStartIndex);
		CachedTrajectory[TrajectoryStartIndex] = Root->GetComponentToWorld();
	}

	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 0, FColor::Green, FString::Printf(TEXT("DirectionAngleDelta = %f"), DirectionDeltaAngle));

	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 0, FColor::White, FString::Printf(TEXT("Ticking leg manager, DirectionDeltaAngle = %f"), DirectionDeltaAngle));
	bIsMoving = !GetOwner<ABasicCharacter>()->MovementComponent->GetVelocity().IsNearlyZero();
	DirectionDeltaAngle = PreviousRotation.Yaw - Root->GetComponentRotation().Yaw;
	PreviousRotation = Root->GetComponentRotation();
	if (MovementComponent->GetVelocity().IsNearlyZero())
	{
		LegMovementState = ELegMovementState::Standing;
	}
	else
	{
		LegMovementState = ELegMovementState::Walking;
	}

	for (auto Leg : Legs)
	{
		Leg->UpdateLeg(DeltaTime);
	}

	FVector LineStart = DEBUGPreviousPosition;
	FVector LineEnd = Root->GetComponentLocation();
	LineStart.Z -= 80;
	LineEnd.Z -= 80;

	DrawDebugLine(GetWorld(), LineStart, LineEnd, FColor::Green, false, 10);

	DEBUGPreviousPosition = Root->GetComponentLocation();

	
}
*/