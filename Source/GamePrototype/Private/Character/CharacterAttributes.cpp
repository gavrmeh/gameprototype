#include "Components/AttributesComponent.h"
#include "Inventory/EquippableItem.h"
#include "Character/BasicCharacter.h"

void UCharacterStateAttribute::Tick(float DeltaTime, UCharacterStateComponent* ParentCharacter)
{
	UBasicAttribute* RegenAttribute = ParentCharacter->GetAttributeByName(RegenAttributeName);
	if (RegenAttribute)
	{
		CurrentValue = FMath::Max<float>(GetValue(ParentCharacter).MaxValue, CurrentValue + RegenAttribute->GetValue(ParentCharacter).CurrentValue*DeltaTime);
	}
}

