#include "Character/Damage.h"
#include "Character/CharacterAttributes.h"
#include "Components/AttributesComponent.h"

float UBasicDamageType::CalculateDamageOutput_Implementation(float BasicDamage, UCharacterStateComponent* Character)
{
    return 0.0f;
}

float UBasicDamageType::CalculateIncomingDamage_Implementation(float IncomingDamage, UCharacterStateComponent* Receiver, UCharacterStateComponent* Instigator)
{
    return 0.0f;
}

float UDefaultDamageType::CalculateDamageOutput_Implementation(float BasicDamage, UCharacterStateComponent* Character)
{
	float FlatDamage = BasicDamage;
	float Scale = 1;
	for (FName ScaleTag : ScalesByTags)
	{
		UBasicAttribute* Attr = Character->GetAttributeByName(ScaleTag);
		if (Attr)
		{
			if (Attr->AttributeType)
			{
				Scale += Attr->GetValue(Character).CurrentValue;
			}
			else
			{
				FlatDamage += Attr->GetValue(Character).CurrentValue;
			}
		}
	}
	return FMath::Max<float>(0, FlatDamage * Scale);
}

float UDefaultDamageType::CalculateIncomingDamage_Implementation(float IncomingDamage, UCharacterStateComponent* Receiver, UCharacterStateComponent* Instigator)
{
	float FlatResistance = 0;
	float PercentageResistance = 0;
	for (FName ResistanceTag : ResistedByTags)
	{
		UBasicAttribute* Attr = Receiver->GetAttributeByName(ResistanceTag);
		if (Attr)
		{
			if (Attr->AttributeType == EAttributeType::Percentage)
			{
				PercentageResistance = Attr->GetValue(Receiver).CurrentValue;
			}
			else
			{
				FlatResistance = Attr->GetValue(Receiver).CurrentValue;
			}
		}
	}
	return FMath::Max<float>(0, IncomingDamage * (1 - PercentageResistance) - FlatResistance);
}
