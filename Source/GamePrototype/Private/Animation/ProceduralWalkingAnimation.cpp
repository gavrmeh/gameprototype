#include "Animation/ProceduralWalkingAnimation.h"
#include "BoneContainer.h"
#include "Engine/Engine.h"
#include "EngineGlobals.h"
#include "TwoBoneIK.h"
#include "FABRIK.h"
#include "Animation/AnimInstanceProxy.h"
#define LOCTEXT_NAMESPACE "A3Nodes"

UProceduralWalkAnimGraphNode::UProceduralWalkAnimGraphNode(const FObjectInitializer& ObjectInitializer)
    :UAnimGraphNode_SkeletalControlBase(ObjectInitializer)
{
}

FLinearColor UProceduralWalkAnimGraphNode::GetNodeTitleColor() const
{
    return FLinearColor::Green;
}

FText UProceduralWalkAnimGraphNode::GetTooltipText() const
{
    return LOCTEXT("ProceduralWalkAnimGraphNode", "ProceduralWalkAnimGraphNode");
}

FText UProceduralWalkAnimGraphNode::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
    return LOCTEXT("ProceduralWalkAnimGraphNode", "ProceduralWalkAnimGraphNode");
}

FString UProceduralWalkAnimGraphNode::GetNodeCategory() const
{
    return TEXT("ProceduralWalkAnimGraphNode");
}

#undef LOCTEXT_NAMESPACE

void FProceduralWalkAnimNode::GatherDebugData(FNodeDebugData& DebugData)
{
    Super::GatherDebugData(DebugData);
}

void FProceduralWalkAnimNode::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
    Super::Initialize_AnyThread(Context);
    AnimInstanceProxy = Context.AnimInstanceProxy;
}

void FProceduralWalkAnimNode::EvaluateSkeletalControl_AnyThread(FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms)
{
    Super::EvaluateSkeletalControl_AnyThread(Output, OutBoneTransforms);
    for (auto& Leg : Legs)
    {
        Leg.InitializeTransforms(AnimInstanceProxy, Output.Pose);
    }
    GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10, FColor::Yellow, TEXT("Evaluating procedural walk anim node"));
    CurrentStepTime += AnimInstanceProxy->GetDeltaSeconds();
    LocalSpaceMovementDirection = AnimInstanceProxy->GetComponentTransform().InverseTransformVector(MovementDir);
    if (CurrentStepTime > TotalStepTime)
    {
        CurrentStepTime = 0;
    }
    for (auto& LegGroup : LegGroups)
    {
        UpdateLegGroup(LegGroup, Output, OutBoneTransforms);
    }
    for (auto& Leg : Legs)
    {
        //Leg.BoneTransforms[Leg.NumBones - 1].SetRotation(FQuat::Identity);
        for (int32 i = 0; i < Leg.NumBones; i++)
        {
            OutBoneTransforms.Push(FBoneTransform(Leg.BoneIndecies[i], Leg.BoneTransforms[i]));
            if (bDrawDebugData)
            {
                AnimInstanceProxy->AnimDrawDebugLine(Leg.IKChainLinks[i].Position, Leg.IKChainLinks[i].Position + Leg.IKChainLinks[i].DefaultDirToParent*Leg.IKChainLinks[i].DefaultDirToParent, FColor::Yellow);
            }
        }
    }
    OutBoneTransforms.Sort(FCompareBoneTransformIndex());
    for (auto& Transform : OutBoneTransforms)
    {
        FVector ArrowEnd = Transform.Transform.GetLocation() + Transform.Transform.GetRotation().Vector()*20;
        AnimInstanceProxy->AnimDrawDebugDirectionalArrow(Transform.Transform.GetLocation(), ArrowEnd, 10,FColor::Red);
    }
    
}

bool FProceduralWalkAnimNode::IsValidToEvaluate(const USkeleton* Skeleton, const FBoneContainer& RequiredBones)
{
    return Legs.Num() > 0 && StepHeightCurve != nullptr;
}

void FProceduralWalkAnimNode::UpdateLegGroup(FLegGroupDescriptor& LegGroup, FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms)
{
    float TotalStepAlpha = CurrentStepTime/TotalStepTime;
    if(TotalStepAlpha > LegGroup.StepStartTime && TotalStepAlpha < LegGroup.StepEndTime )
    {
        float StepAlpha = (CurrentStepTime - LegGroup.StepStartTime)/(LegGroup.StepEndTime - LegGroup.StepStartTime);
        for (uint32 LegID : LegGroup.LegIndecies)
        {
            UpdateLegStepTarget(Legs[LegID]);
            UpdateLegStep(Legs[LegID], StepAlpha, Output, OutBoneTransforms);
        }
    }
    else
    {
        for(uint32 LegID : LegGroup.LegIndecies)
        {
            UpdateLegKeepPosition(Legs[LegID], Output, OutBoneTransforms);
        }
    }
    
}

FProceduralWalkAnimNode::FProceduralWalkAnimNode()
{
}

void FProceduralWalkAnimNode::UpdateLegStep(FLegDescriptor& Leg, float StepAlpha, FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms)
{
    UpdateLegStepTarget(Leg);
    
    Leg.ComponentSpaceCurrentPosition = FMath::Lerp(Leg.ComponentSpaceLastPlantPosition, Leg.ComponentSpaceStepTarget, StepAlpha);
    Leg.ComponentSpaceCurrentPosition.Z += StepHeightCurve->GetFloatValue(StepAlpha)*MaxStepHeight;

    AnimationCore::SolveFabrik(Leg.IKChainLinks, Leg.ComponentSpaceCurrentPosition, Leg.MaximumReach, 0.01, 12);
    for (int32 i = 0; i < Leg.NumBones; i++)
    {
        Leg.BoneTransforms[i].SetLocation(Leg.IKChainLinks[Leg.NumBones - i - 1].Position);
    }
    UpdateBoneTransforms(Leg, Output, OutBoneTransforms);
    AnimInstanceProxy->AnimDrawDebugSphere(Leg.ComponentSpaceCurrentPosition, 10, 16, FColor::Cyan);
}
void FProceduralWalkAnimNode::UpdateLegKeepPosition(FLegDescriptor& Leg, FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms)
{
    Leg.ComponentSpaceCurrentPosition-=AnimInstanceProxy->GetSkelMeshCompLocalToWorld().InverseTransformVector(MovementDir);
    AnimationCore::SolveFabrik(Leg.IKChainLinks, Leg.ComponentSpaceCurrentPosition, Leg.MaximumReach, 0.01, 12);
    for (int32 i = 0; i < Leg.NumBones; i++)
    {
        Leg.BoneTransforms[i].SetLocation(Leg.IKChainLinks[Leg.NumBones - i - 1].Position);
    }
    UpdateBoneTransforms(Leg, Output, OutBoneTransforms);
}

void FProceduralWalkAnimNode::UpdateLegStepTarget(FLegDescriptor& Leg)
{
    AActor* OwningActor = AnimInstanceProxy->GetSkelMeshComponent()->GetOwner();
    //Calculate step target
    FVector DefaultWorldSpacePosition = ConvertToWorldSpace(Leg.ComponentSpaceDefaultFootPosition);
    FVector RayStart = DefaultWorldSpacePosition;
    RayStart.Z += MaxStepHeight;
    FVector RayEnd = RayStart + MovementDir * AnimInstanceProxy->GetDeltaSeconds() * (CurrentVelocity / MaxStepDistanceSpeed) * StepForwardDistance;
    
    FHitResult Result;
    FCollisionQueryParams CollisionQueryParams;
    CollisionQueryParams.AddIgnoredActor(OwningActor);
    OwningActor->GetWorld()->LineTraceSingleByChannel(Result, RayStart, RayEnd, OwningActor->GetRootComponent()->GetCollisionObjectType(), CollisionQueryParams);
    

    if (!Result.IsValidBlockingHit())
    {
        FVector SecondaryRayEnd = RayEnd;
        SecondaryRayEnd.Z -= MaxStepHeight;
        OwningActor->GetWorld()->LineTraceSingleByChannel(Result, RayEnd, SecondaryRayEnd, OwningActor->GetRootComponent()->GetCollisionObjectType(), CollisionQueryParams);
    }
    if (Result.IsValidBlockingHit())
    {
        Leg.ComponentSpaceStepTarget = ConvertToComponentSpace(Result.ImpactPoint);
    }
}


void FProceduralWalkAnimNode::InitializeBoneReferences(const FBoneContainer& RequiredBones)
{
    for (FLegDescriptor& Leg : Legs)
    {
        int32 LegBoneIndex = 0;
        Leg.BoneIndecies.Reset();
        Leg.BoneIndecies.Reserve(Leg.NumBones);
        FBoneReference FootBoneReference(Leg.FootBoneName);
        FootBoneReference.Initialize(RequiredBones);
        FCompactPoseBoneIndex CurrentBoneIndex = RequiredBones.GetCompactPoseIndexFromSkeletonIndex(FootBoneReference.BoneIndex);
        while (LegBoneIndex < Leg.NumBones)
        {
            Leg.BoneIndecies.Push(CurrentBoneIndex);
            CurrentBoneIndex = RequiredBones.GetParentBoneIndex(CurrentBoneIndex);
            LegBoneIndex++;
        }
    }
}

FTransform FProceduralWalkAnimNode::ConvertToWorldSpaceTransform(const FTransform& ComponentSpaceTransform)
{
    return AnimInstanceProxy->GetSkelMeshCompLocalToWorld()*ComponentSpaceTransform;
}

FTransform FProceduralWalkAnimNode::ConvertToComponentSpaceTransform(const FTransform& WorldSpaceTransform)
{
    return AnimInstanceProxy->GetSkelMeshCompLocalToWorld().Inverse()*WorldSpaceTransform;
}

FVector FProceduralWalkAnimNode::ConvertToWorldSpace(const FVector& ComponentSpacePosition)
{
    return AnimInstanceProxy->GetSkelMeshCompLocalToWorld().TransformPosition(ComponentSpacePosition);
}

FVector FProceduralWalkAnimNode::ConvertToComponentSpace(const FVector& WorldSpacePosition)
{
    return AnimInstanceProxy->GetSkelMeshCompLocalToWorld().InverseTransformPosition(WorldSpacePosition);
}

void FProceduralWalkAnimNode::UpdateBoneTransforms(FLegDescriptor& Leg, FComponentSpacePoseContext& Output, TArray<FBoneTransform>& OutBoneTransforms)
{
    for (int32 i = 1; i < Leg.NumBones; i++)
    {
        FTransform OldCurrentTransform = Output.Pose.GetComponentSpaceTransform(Leg.BoneIndecies[i]);
        FTransform OldChildTransform = Output.Pose.GetComponentSpaceTransform(Leg.BoneIndecies[i-1]);
        FTransform& CurrentTransform = Leg.BoneTransforms[i];
        FTransform& ChildTransform = Leg.BoneTransforms[i - 1];

        FVector OldDir = (OldChildTransform.GetLocation() - OldCurrentTransform.GetLocation()).GetSafeNormal();
        FVector NewDir = (ChildTransform.GetLocation() - CurrentTransform.GetLocation()).GetSafeNormal();


        float Angle = FMath::Acos(FVector::DotProduct(OldDir, NewDir));
        FVector Axis = FVector::CrossProduct(OldDir, NewDir).GetSafeNormal();
        FQuat DeltaRotation = FQuat(Axis, Angle);
        DeltaRotation.Normalize();
        check(DeltaRotation.IsNormalized());
        CurrentTransform.SetRotation(OldCurrentTransform.GetRotation()*DeltaRotation);
        //AnimInstanceProxy->AnimDrawDebugDirectionalArrow(CurrentTransform.GetLocation(), CurrentTransform.GetLocation() + 20*NewDir, 5, FColor::Emerald);
    }
}

void FLegDescriptor::InitializeTransforms(FAnimInstanceProxy* MyAnimInstanceProxy, FCSPose<FCompactPose>& MeshBases)
{
    const FBoneContainer& RequiredBones = MeshBases.GetPose().GetBoneContainer();
    ComponentSpaceDefaultFootPosition = MeshBases.GetComponentSpaceTransform(BoneIndecies[0]).GetLocation();
    IKChainLinks.Reset();
    BoneTransforms.Reset();
    IKChainLinks.SetNum(NumBones);
    BoneTransforms.SetNum(NumBones);
    MaximumReach = 0;
    //We build IK chain from root to foot bone since FABRIK expects it
    for (int32 i = 0; i < NumBones - 1; i++)
    {
        auto CurrentIndex = BoneIndecies[i];
        auto ParentIndex = BoneIndecies[i+1];
        auto& CurrentLink = IKChainLinks[NumBones - i - 1];
        CurrentLink.BoneIndex = CurrentIndex.GetInt();
        FVector ParentToCurrent = MeshBases.GetComponentSpaceTransform(ParentIndex).GetLocation() - MeshBases.GetComponentSpaceTransform(CurrentIndex).GetLocation();
        CurrentLink.DefaultDirToParent = ParentToCurrent.GetUnsafeNormal();
        CurrentLink.Length = ParentToCurrent.Size();
        CurrentLink.Position = MeshBases.GetComponentSpaceTransform(CurrentIndex).GetLocation();
    }
    auto& RootLink = IKChainLinks[0];
    RootLink.Position = MeshBases.GetComponentSpaceTransform(BoneIndecies[NumBones - 1]).GetLocation();
}

