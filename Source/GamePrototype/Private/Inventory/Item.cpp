#include "Inventory/Item.h"
#include "Character/BasicCharacter.h"
#include "GameFramework/PlayerState.h"

UTexture2D* AItem::GetThumbnail() const
{
    return Thumbnail;
}

void AItem::Interact_Implementation(APlayerState* Player)
{
    APawn* Character = Player->GetPawn();
    UCharacterStateComponent* CharacterState =
        reinterpret_cast<UCharacterStateComponent*>(Character->GetComponentByClass(UCharacterStateComponent::StaticClass()));
    UContainerComponent* InventoryComponent = CharacterState->GetContainer();
    if (InventoryComponent)
    {
        if(CanBeStacked())
        {
            AItem* OtherItem = InventoryComponent->GetItemByOtherItemName(this);
            if (OtherItem)
            {
                int32 Overstack = OtherItem->Quantity + Quantity - OtherItem->MaxItemsInStack;
                if (Overstack > 0)
                {
                    OtherItem->Quantity = OtherItem->MaxItemsInStack;
                    InventoryComponent->Items.Add(this);
                    Quantity = Overstack;
                    GetWorld()->RemoveActor(this, true);
                }
                else
                {
                    OtherItem->Quantity+=Quantity;
                    Destroy();
                }
            }
            else
            {
                InventoryComponent->Items.Add(this);
            }
        }
        else
        {
            InventoryComponent->Items.Add(this);

        }
        bIsInWorld = false;
    }
}
