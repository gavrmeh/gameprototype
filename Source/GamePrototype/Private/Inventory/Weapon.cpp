
#include "Inventory/Weapon.h"

FDamage AWeapon::GetDamage_Implementation() const
{
	FDamage Result;
	return FDamage();
}

GAMEPROTOTYPE_API uint32 GetTypeHash(const FWeaponDamage& Damage)
{
	return GetTypeHash(Damage.BaseDamageValue) ^ GetTypeHash(Damage.DamageType);
}
