#include "Inventory/EquippableItem.h"
#include "Components/AttributesComponent.h"
void AEquippableItem::OnEquip(UCharacterStateComponent* Character)
{
	Character->ApplyModifiers(Modifiers);
	OwningCharacter = Character;
	bIsEquipped = true;
}

void AEquippableItem::OnUnequip()
{
	OwningCharacter->RemoveModifiers(Modifiers);
	OwningCharacter = nullptr;
	bIsEquipped = false;
}
